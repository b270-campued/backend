// alert("Hello, Battch 270!");

// This is a statement
// console.log displays data what we are dealing or got
console.log("Hello again!");

// JS without ; is okay since JS is loose language or not strict but not good practice
console.log("Hi, Batch 270!")

/*
	-There are two types of comments:
	1. Single Line (CTRL + /)
	2. Multi-Line (CTRL + SHIFT + /)

*/

// [SECTION] Variables
	/*
		- It is used to contain data.
	*/
// Declaring Variables
// Syntax: let/const/var variableName;

/*
	-Trying to print out a value of variable that has not been declared will return an error of undefined
	-This means variable must be declared value before they can be used
*/
	let myVariable;
	console.log(myVariable);
//undefined error means no value yet
// Also take note if theres error in a line succeeding codes won't continue (top to bottom rule)
	console.log("myVariable");
// This return in console as myVariable as its considered string/text as it use ""


// Cannot access 'greeting' before initialization as JS is read from top to bottom 
	// console.log(greetings)
	// let	greetings = "Hi";
	
// Correct one declared value then print out using log
	let	message = "Hello"
	console.log(message);

// Declaring and initializing variables
	// Initializing variables - the instance when a variable is given its initial value while declaring is just the let/const/var without value
	let productName = 'desktop computer'
	// Reassigning let keyword value
	productName = "Laptop" 
	console.log(productName);

	let productPrice = 18999
	console.log(productPrice);

	const interest = 3.539
	// interest = 2.5 will have an error reassignment to const value
	console.log(interest);

// let can reassign value const is fixed and value can't be changed

	let friend = "Kate"
	console.log(friend)
// let friend = "Jay" wil return error friend already declared (we cannot re-declare variables within its scope)
	friend = "Jay"
	console.log(friend)

// [SECTION] Data Types

	// String - series of characters that creates a word, phrase, sentence or anything related to creating a text
	// Strings can ve written using either a single or double quote
	let country = "Phlippines"
	let province = "Batangas"
	console.log(country)
	console.log(province)

// Concatenate Strings
// Multiple string values can be combined to create a single string using + symbol (concatenate)
let fullAddress = province + "," + country
console.log(fullAddress)
console.log("I live in the " + country)

// OR

let address = "I live in the " + country;
console.log(address);

// escape character /
let mailAddress = "Metro Manila\n\nPhilippines"
console.log(mailAddress)


let message1 = "John's employess went home early.";
console.log(message1);

// Numbers
	// Integers/Whole numbers
	let headCount = 26
	console.log(headCount);

	// Decimal Numbers 
	let grade = 98.7
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10
	console.log(planetDistance)

// Boolean 

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + inGoodConduct)
console.log("inGoodConduct: " + inGoodConduct)

// Arrays - are special kind of data type that's used to store multiple data types
// Arrays can store different data types but is normally used to store similar data types
// Syntax: let/const arrayname = [elementA, elementB, ...]
// Arrays are in plural keywords and have index/order/sequence of value which start at index 0

let grades = [98.7,92.1,90.2,94.6];
console.log(grades);

let details = ["John", "Smith", 32, true]
console.log(details)

// Objects - it have properties and values means it more descriptive than arrays
/* Syntax: let/const objectName = {
	propertyA: ValueA,
	propertyB: ValueB
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "09123456789"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};
console.log(person);

// Null
let spouse = null;
console.log(spouse);