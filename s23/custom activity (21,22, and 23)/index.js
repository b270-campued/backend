let myString = "hello world";
let myArray = myString.split('');

for (let i = 1; i < myArray.length; i += 2) {
  myArray[i] = 'O';
}

let modifiedString = myArray.join('');
console.log(modifiedString); // hOlO wOrld
