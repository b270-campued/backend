// console.log("Welcome back, Batch 270!")

// An object can contain another object and functions
// Object uses properties vs arrays that uses indexes
// Objects have context on his values (key-values structure)

//[SECTION] Objects 
/*
	-An object is data type that is used to represent real world objects
	-Information is stored and represented in a key(property)-value pair 
*/

//Creating objects using initializers/literal notation
/*
	-This creates/declares an object amd also initialize/assigns its properties
	upon creating 
	-A cellphone is an example of real world object that have properties like
	name, color, price, ec.
	-Syntax : 
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufacturerDate: 1999
};

console.log("Result from creating initializers/literal notation:");
console.log(cellphone);
console.log(typeof cellphone);

//Creating objects using a constructor function

/*
	-Creates a reusable function who create several objects that have the same
	data structure
	-An "instance" is a concrete occurence of any object which emphasize on the
	distinct/unique identity of it
	Syntax:
		function objectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB
		}
*/

// The "this" keyword allows us to assign a new objects property by associating them with the 
//values received from a constructor functions parameters
function Laptop(name, manufacturerDate) {
	this.name = name;
	this.manufacturerDate = manufacturerDate;
};


//This is the unique instance of Laptop object
// The "new" operator creates an instance of an object
let laptop = new Laptop("Lenovo", 2008)
console.log("Result of creating objects using object constructors: ");
console.log(laptop); 
//This is another unique instance of Laptop object
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result of creating objects using object constructors: ");
console.log(myLaptop);

//This only invoke/calls the 'Laptop' function instead of creating a new object instance
let oldLaptop = Laptop("Acer", 1980);
console.log("Result of creating objects without using the new keyword: ");
console.log(oldLaptop); // will return undefined as it creates object without creating a new instance


//Creating empty objects in 2 ways
let computer = {};
console.log(computer);

let myComputer = new Object();
console.log(myComputer);

// [SECTION] Accessing Properties inside Object

// Using the dot notation (commonly used)

console.log("Result from dot notation: " + myLaptop.name);

//Using using the square bracket notation
console.log("Result from square bracket notation: " + myLaptop["name"]);


// [SECTION] Initalizing/Adding/Deleting/Reassigning Object Properties

let car = {};
console.log(car);

//Initializing/Adding object properties using dot notation
car.name = "Honda Civic";
console.log("Result of adding properties using dot notation: ");
console.log(car);

car["manufacturerDate"] = 2019;
console.log("Result of adding properties using square bracket notation: ");
console.log(car);

//Reassigning object properties

car.name = "Ford Raptor";
console.log("Result of reassigning properties: ");
console.log(car);

//Deleting object properties
delete car["manufacturerDate"]
console.log("Result of deleting properties: ");
console.log(car);


// [SECTION] Object Methods

/*
	- a method is a function propertyy of an object
	- methods are useful for creating object specific function which are used
	to perform task on them
*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person);
console.log("Result of object methods: ");
person.talk(); //invoke talk function using dot notation

// Adding methods to objects

person.walk = function () {
	console.log(this.name + " walked 25 steps forward.");
}

person.walk();
console.log(person);

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com", "joesmith@email.xyz"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();

/*
	Scenario:
	1. We would like to create a game that would have several pokemon
	interact with each other.
	2. Every pokemon would have the same set of stats, properties and functions.
*/


//Create multiple kinds of pokemon
// Use initialization/literal - would be time consuming
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonhealth")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}

console.log(myPokemon);

// Use Object Constructor

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonhealth")
	};
	this.faint = function() {
		console.log(this.name + "fainted.")
	}
}

// Creates new instance of pokemon

let pikachu = new Pokemon("Pikachu", 16);
console.log(pikachu);
let rattata = new Pokemon("Rattata", 8);
console.log(rattata);

pikachu.tackle(rattata);

rattata.tackle(pikachu);