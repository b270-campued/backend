// console.log("Gotta Catch Em All!");

// 3. Create a trainer object using object literals.
let trainer = {
  // 4. Initialize/add the following trainer object properties:
  name: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
  	hoenn: ["May", 'Max'],
  	kanto: ["Brock", "Misty"]
  },
  // 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
  talk: function() {
    console.log("Pikachu! I choose you!");
  },
};

console.log(trainer);
// 6. Access the trainer object properties using dot and square bracket notation.
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracker notation:");
console.log(trainer.pokemon);
// 7. Invoke/call the trainer talk object method.
console.log("Result of talk method");
trainer.talk();

// 8. Create a constructor for creating a pokemon with the following properties:
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level * 1;
  // 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
  this.tackle = function(targetPokemon) {
	  targetPokemon.health -= this.attack;
	  console.log(this.name + ` tackled ${targetPokemon.name}`);

		  // 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
		  if (targetPokemon.health <= 0) {
		    console.log(`${targetPokemon.name} is now reduced to ${targetPokemon.health}.`);
		    targetPokemon.faint();
		  } else {
		    console.log(`${targetPokemon.name} is now reduced to ${targetPokemon.health}.`);
		  }

  };

  // 11. Create a faint method that will print out a message of targetPokemon has fainted.
  this.faint = function() {
  console.log(`${this.name} has fainted.`);
  };
}

// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

console.log(geodude);
