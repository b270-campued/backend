// console.log("Hello World!")
// Array is a list of multiple data stored inside a variable var = []

// [SECTION] Arrays
// An array in programming is simply a list of data
// They are declared using the square bracker ([]) also know "Array Literals"

let studentNumberA = "2023-1923"
let studentNumberB = "2023-1924"
let studentNumberC = "2023-1925"
let studentNumberD = "2023-1926"
let studentNumberE = "2023-1927"

// arrays variable should be plural since its a set of data
let studentNumbers = ["2023-1923", "2023-1924", "2023-1925", "2023-1926", "2023-1927"];
console	.log(studentNumbers);

let grades = [71, 100, 85, 90];
console.log(grades);

let computerBrands = ["Acer", "Lenovo", "Dell", "Asus", "Apple", "Huawei"];
console.log(computerBrands);

// This type still possible use of array BUT IS NOT RECOMMENDED
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr); //suggested with same data type to have same context by just looking at variable name.

// Alternative Way to Write Arrays

let myTask = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake bootstrap"
];
console.log(myTask);

// Creating an array with values from variables:

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Nirobi";
let city4 = "Rio";

let cities = [city1, city2, city3, city4];
console.log(cities);

// [SECTION] .length property
//return the count of the elements(array) and characters(string)

console.log(myTask.length);
console.log(cities.length);

let blankArr = []
console.log(blankArr.length);

let fullName = "Franz Campued";
console.log(fullName.length); //also counts the spaces

// length property can also set the total number of items in an array, meaning we can delete
//the lst item in the array or shorten the array by simply updating the length property of array

myTask.length = myTask.length-1; //take note method here is reassigned to .lenght
console.log(myTask.length);
console.log(myTask);

cities.length--;
console.log(cities);

//above rule for arrays does not work on strings
fullName.length--;
console.log(fullName.length);
console.log(fullName);

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);

theBeatles.length++; //will add empty element
console.log(theBeatles) 

// [SECTION] Reading from Arrays
// you can access them through index starting with 0 

console.log(grades[0]);
console.log(computerBrands[3]);

// Accessing an array element that does not exist will return undefined
console.log(grades[20]);

let lakersLegends = ["Kobe", "Lebron", "Shaq", "Magic", "Kareem"];

console.log(lakersLegends[3]);
console.log(lakersLegends[1]);
console.log(lakersLegends[2]);

// You can also store array items inside another variable
let currentLaker = lakersLegends[1];
console.log(currentLaker);

console.log("Array before re-assignment");
console.log(lakersLegends)
lakersLegends[2] = "Davis" //reassign using index
console.log("Array after re-assignment");
console.log(lakersLegends)


// Accessing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]
						//5-1 - since index starts at 0
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex])
						//5-1 - since index starts at 0 - here though its direct
console.log(bullsLegends[bullsLegends.length-1]);

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife" //add using index
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

// adding new item at the end of new array
newArr[newArr.length] = "Barret Wallace"; //array length count normal but index start at zero so using newArr.length it already means accessing and  adding new index and its value
console.log(newArr);


// Loopinh over an arrayy
for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++) {
	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}
}


// [SECTION] Multidimensional Arrays
// Useful for storing complex data structures


let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];
console.table(chessBoard);
					// [row] and [column]
console.log(chessBoard[1][4]); //e2
console.log(chessBoard[7][7]); //h8

console.log("Pawn moves to: " + chessBoard[1][5]); //f2

