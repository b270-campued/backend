// SEPARATION OF REQUEST (Modules, and Parameterized Routes)

/*
	Why do we have to? (1 file for each controller, routes, models)
	-Better code readability
	-Improve scalability
	-Better code maintainability

	Models:
	-

	Controllers:
	-Contains instructions HOW your API will perform its intended task
	-Mongoose model queries are used here. e.g:
	 -Model.find()
	 -Model.findByIdAndDelete()
	 -Model.findByIdAndUpdate()

	Routes:
	-Defines WHEN particular controllers will be used
	-A specific controller actio will be called when specific HTTP method is
	received on a specific API endpoint


	We separate our concers through JS modules.
	Modules can be use and reuse in other parts of our application.

*/

//index.js
// npm init -y (Creating package.json)
// npm install express mongoose (express and mongoose package)
// .gitignore of node_modules

// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//Allows us to use all the routes defined in "taskRoutes.js"
// Inside "" is location path of task (.. = means going out)
const taskRoutes = require("./routes/taskRoutes")

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.zmpg8k6.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We are connected to database"));


// Add the task route
//Allows all the task routes created in the "taskRoutes.js" file to us "/tasks" route
app.use("/tasks", taskRoutes)

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

// index.js will only have these codes
// create new folder and taskRoutes.js inside for routes endpoints

//Flow
/*
	index -> connected to routes as servers should contain routes/endpoints
	routes -> connected to controller as controllers contains instructions HOW your API will perform its intended task
	controller -> connected models/schemas 
*/


/*
[CODE EXPLAINATION]

This is a Node.js script that sets up a simple server using the Express.js framework, connects to a MongoDB database using Mongoose, and defines routes for handling HTTP requests to a /tasks endpoint.

Here's a step-by-step explanation of what the script does:

1. First, it imports the necessary modules: Express and Mongoose. It also imports a module called taskRoutes, which is a separate file that defines the specific routes for handling HTTP requests related to tasks.

2. It creates an instance of the Express application and sets the port number to 3001.

3. It sets up middleware to parse incoming JSON and URL-encoded data.

4. It connects to a MongoDB database hosted on MongoDB Atlas using the Mongoose library. The connection string includes the username/admin, password/admin123, and name of the database/b270-to-do to connect to.

5. It sets up event listeners(.on and .once) for the database connection to log any errors or a success message.

6. It sets up the /tasks route to handle incoming requests. This route is defined in the separate taskRoutes module that was imported earlier.

-----
In a Node.js and Express application, `app.use()` is a method that adds middleware functions to the application's request-response cycle. Middleware functions are functions that have access to the request object (`req`), the response object (`res`), and the next middleware function in the application's request-response cycle (`next`).

In this case, `app.use("/tasks", taskRoutes)` adds a middleware function that mounts the `taskRoutes` router at the `/tasks` route. 

`taskRoutes` is an Express router that handles requests and responses related to the "Task" resource of the application. 

By mounting the `taskRoutes` router at the `/tasks` route, any request that starts with `/tasks` will be handled by the `taskRoutes` router.

For example, if the application receives a GET request to `/tasks`, Express will pass that request to the `taskRoutes` router to handle. The router might have a handler function that fetches all tasks from the database and sends them back as a JSON response.
------

7. It starts the server listening on the specified port number.

------
In a Node.js and Express application, app.listen() is a method that starts a server that listens for incoming requests on a specified port.

When you call app.listen(port), the application starts a server and binds it to the specified port number. This means that the server is now ready to receive HTTP requests sent to that port.

For example, if you call app.listen(3000), the application will start a server that listens on port 3000. If a client sends an HTTP request to port 3000 on the server, the server will receive that request and will pass it to the appropriate middleware function(s) to handle the request and generate a response.


The `app.listen()` method in a Node.js and Express application takes two parameters:

1. `port` - the port number that the server will listen on. This parameter is required.

2. `callback` - an optional callback function that will be called once the server has started listening for incoming requests. This parameter is optional, but it's common to include it so that you can log a message to the console or perform other setup tasks.

------

When the script runs, it logs a message indicating that the server is listening on the specified port. This means that the server is now ready to receive and handle HTTP requests at the /tasks endpoint.

*/