// Creating the schema/blueprint, model and export the file

const mongoose = require("mongoose");


/*
	mongoose.Schema here is constructor

	`mongoose.Schema()` is a constructor function in the Mongoose library that is used to 
	create a schema object for a MongoDB collection. A schema is a blueprint for the 
	structure of the documents in a collection, and it defines the fields, data types, and 
	other options that should be enforced when creating and updating documents in the collection.
*/

/*
See this old topic:

[SECTION] Class-Based Object Blueprints


	- The constructor is a special method of a class for 
	creating/initializing an object for that class
	Syntax:
	class className {
		constructor(objectPropertyA, objectPropertyB) {
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;

		}
	}

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

-- Instantiating a new object with initialized values

const myNewCar = new Car("Toyota", "Fortuner", 2020);
console.log(myNewCar);

*/

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// "Task" here is the collection where the schema is followed
module.exports = mongoose.model("Task", taskSchema)

/*
[CODE EXPLANATION]


This is a Node.js script that defines a Mongoose schema for a "Task" collection in a MongoDB database.

Here's a step-by-step explanation of what the script does:

1. First, it imports the Mongoose library.

2. It defines a new Mongoose schema using the `mongoose.Schema` method. The schema has two properties: `name` and `status`. The `name` property is a simple string field, while the `status` property is a string field with a default value of "pending". 

3. The `module.exports` statement makes this schema available for use in other parts of the application. Specifically, it exports a Mongoose model that can be used to interact with the "Task" collection in the MongoDB database. The first argument to `mongoose.model` is the name of the model, which is "Task" in this case. The second argument is the schema that defines the shape of the documents in the collection.

By exporting this schema as a Mongoose model, other parts of the application can create, read, update, and delete documents in the "Task" collection using Mongoose's built-in methods. For example, to create a new document in the "Task" collection, you could use the following code:

```
const Task = require("./models/task"); // import the task model

const newTask = new Task({
    name: "Buy groceries",
    status: "pending"
});

newTask.save((err, result) => {
    if (err) {
        console.error(err);
    } else {
        console.log(result);
    }
});
```

This code creates a new instance of the Task model with a name of "Buy groceries" and a default status of "pending". The `save` method then inserts the new document into the "Task" collection in the MongoDB database.


[mongoose.model() method]

The `mongoose.model()` method takes two parameters:

1. The first parameter is a string that represents the name of the collection that the model represents. 

For example, in the code `mongoose.model("Task", taskSchema)`, `"Task"` is the name of the collection. This parameter is important because Mongoose uses it to determine the name of the collection in the MongoDB database.

If the first parameter is not provided, Mongoose will attempt to automatically pluralize the name of the model to determine the name of the collection. For example, if the model name is "Task", Mongoose will look for a collection called "tasks" in the database.

2. The second parameter is a Mongoose schema that defines the shape of the documents that will be stored in the collection. 

In the code `mongoose.model("Task", taskSchema)`, `taskSchema` is the schema that defines the structure of the documents in the "Task" collection. 

The schema is an object that specifies the fields that each document in the collection should have and the types of those fields. The schema can also specify default values, validation rules, and other constraints on the data.

Once a model has been defined using `mongoose.model()`, it can be used to perform various CRUD (Create, Read, Update, Delete) operations on the collection it represents, using Mongoose's built-in methods such as `find()`, `findOne()`, `create()`, `updateOne()`, and `deleteOne()`.


---See previous topics for mongoDB query and operations
*/