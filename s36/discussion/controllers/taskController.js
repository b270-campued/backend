// Controllers contain the functions and business logic of our Express application
// All the operations that is can do will be places in this file
// Inside "" is location path of task (.. = means going out)
// task here in not case sensitive we can use small t

const Task = require("../models/task")


//Create a getAllTasks function
module.exports.getAllTasks = () => {
	//If parameter is empty it means all
	// Compared to s35 error is not coded or taken into consideration

	return Task.find({}).then(result => {
		return result
	})
}

/*
	This code exports a function called `getAllTasks()` using the `module.exports` syntax, 
	which can be used by other parts of the application to retrieve all tasks from the database. 

	The function first uses the `find()` method from the `Task` model to retrieve all documents 
	from the `Task` collection in the database. The empty object `{}` passed as the first argument 
	to `find()` means that all documents will be returned, as no specific criteria for 
	matching are provided.

	Once the documents are retrieved, the function returns them using a `then()` method call, 
	which creates a promise that will be resolved with the retrieved documents.

	***It's worth noting that the function does not take any parameters, so it will always return 
	all tasks from the database. Additionally, there is no error handling in this code, 
	so any errors that occur during the `Task.find()` operation will not be handled, 
	and the promise returned by `getAllTasks()` will be rejected.
*/

// Create a createTask function
module.exports.createTask = (requestBody) => {

	//Creates a task object based on the model Task
	let newTask = new Task({

		// Sets the name property with the value received fro the client/postman
		name: requestBody.name

	})

	return newTask.save().then((task, error) => {

		if (error) {
			console.log(error);
			return false
		} else {
			return task;
		}
	})
}

/*
	This code exports a function called `createTask()`, which accepts a single parameter 
	`requestBody`. The purpose of the function is to create a new task in the database, 
	using the `Task` model and the information provided in the `requestBody` parameter.

	The function first creates a new `Task` object, setting its `name` property to the value 
	provided in the `requestBody` parameter.

	Once the `Task` object is created, the function calls its `save()` method to save it 
	to the database. This returns a promise that will be resolved with the saved `Task` object 
	if the operation is successful.

	If there is an error during the save operation, the promise will be rejected with the error. 
	In this case, the function logs the error to the console and returns `false` to indicate 
	that the operation was not successful.

	If there is no error during the save operation, the promise will be resolved with the 
	saved `Task` object, which the function returns to the caller.

	It's worth noting that the function assumes that the `requestBody` parameter contains a `name` 
	property that is used to set the `name` property of the new `Task` object. 
	If this assumption is incorrect, the function will not behave as intended. 
	
	***Additionally, there is no validation or error handling for cases where the `requestBody` parameter 
	is missing or malformed.
*/

// Create a deleteTask function (taskId = parameter)
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err);
			return false
		} else {
			return removedTask;
		}
	})
}

/*
	This code exports a function called `deleteTask()`, which accepts a single parameter `taskId`. 
	The purpose of the function is to remove a task from the database with the given `taskId` 
	by using the `Task` model.

	The function first calls the `findByIdAndRemove()` method on the `Task` model, 
	passing in the `taskId`. This method will find the document with the specified ID 
	and remove it from the collection. The method returns a promise that will be resolved 
	with the removed `Task` object if the operation is successful.

	If there is an error during the remove operation, the promise will be rejected with the error. 
	In this case, the function logs the error to the console and returns `false` to indicate that 
	the operation was not successful.

	If there is no error during the remove operation, the promise will be resolved with 
	the removed `Task` object, which the function returns to the caller.

	***It's worth noting that the function assumes that the `taskId` parameter is a valid ID for 
	a document in the `Task` collection. If the `taskId` is not a valid ID, 
	the `findByIdAndRemove()` method will return `null`, which will be resolved by the promise 
	with a value of `null`. Additionally, there is no error handling for cases where the 
	`taskId` parameter is missing or malformed.
*/

// Create a updateTask function (taskId = parameter)
module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} 

		result.name = newContent.name;

		// Result of "findById" will be stored in the "result" parameter
		// It's "property" will be reassigned to the value of the "name" received
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}

/*
	This code exports a function called `updateTask()`, which accepts two parameters: 
	`taskId` and `newContent`. The purpose of the function is to update the content of a 
	task with the given `taskId` by using the `Task` model.

	The function first calls the `findById()` method on the `Task` model, passing in the `taskId`. 
	This method will find the document with the specified ID and return it as a promise. 
	If the operation is successful, the promise will be resolved with the `Task` object.

	If there is an error during the find operation, the promise will be rejected with the error. 
	In this case, the function logs the error to the console and returns `false` to indicate 
	that the operation was not successful.

	If there is no error during the find operation, the function updates the 
	`name` property of the `result` object with the `name` value from the `newContent` object.

	The function then calls the `save()` method on the `result` object, which will save the 
	updated document to the database. The `save()` method returns a promise that will be 
	resolved with the updated `Task` object if the operation is successful.

	If there is an error during the save operation, the promise will be rejected with the error. 
	In this case, the function logs the error to the console and returns `false` to indicate that 
	the operation was not successful.

	If there is no error during the save operation, the promise will be resolved 
	with the updated `Task` object, which the function returns to the caller.

	It's worth noting that the function assumes that the `taskId` parameter is a valid ID 
	for a document in the `Task` collection, and that the `newContent` parameter contains a 
	`name` property. If the `taskId` is not a valid ID or if the `newContent` object does not 
	contain a `name` property, the function may throw an error or return unexpected results.

*/

// [SECTION] Activity
//Create a getSpecificTasks function
module.exports.getSpecificTasks = (taskId) => {
	
	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			return result
		}
	})
}

// Create a taskCompleted function 
module.exports.taskCompleted = (taskId/*, newStatus*/) => {

	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} 

		// /**/ inside the comment will be used if we want to have input in body
		result.status = /*newStatus.status*/"completed";

		return result.save().then((completedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return completedTask;
			}
		})
	})
}