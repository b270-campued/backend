// Contains all endpoints for our application

const express = require("express");

// Inside "" is location path of task (.. = means going out)
//The "taskController" allows us to use the functions defined in the "taskController.js"
const taskController = require ("../controllers/taskController");

// Creates a Router instance that function as a middleware and routing system
// Allows access to HTTP methods middlewares that makes it easier to create routes
const router = express.Router();

// [SECTION] Routes

// Route to get all the task
router.get("/", (req, res) => {

	// Invokes the "getAllTasks" function from the "taskController.js" file
	// "resultFromController" is a parameter
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

/*
	Addtl note in arrow function:

	--> When there is only one parameter name you can omit the parentheses 
	around the parameter list e.g resultFromController is a parameter
	--> An arrow function has an implicit return value when the curly 
	braces is omitted
	
*/

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})

// Route to delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(
		resultFromController));
})

// Route to update a task
router.put("/:id", (req, res) => {
	//We got 2 arguments (req.params.id, request.body)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(
		resultFromController));
})

// [SECTION] ACTIVITY 

router.get("/:id", (req, res) => {
	taskController.getSpecificTasks(req.params.id).then(resultFromController => res.send(
		resultFromController));
})

// Route to mark a task completed
router.put("/:id/complete", (req, res) => {
	// /**/ inside the comment will be used if we want to have input in body
	taskController.taskCompleted(req.params.id/*, req.body*/).then(resultFromController => res.send(
		resultFromController));
})

// Exports the router object to be used in the "index.js"
module.exports = router;


/*
[CODE EXPLANATION]

This code exports a module that creates an instance of an Express Router. 
The Router provides a way to create modular, mountable route handlers that can be used in an Express application. 

The Router has various HTTP method functions, 
such as `get()`, `post()`, `put()`, and `delete()`, 
which allow you to define routes that respond to specific HTTP requests. 

In this code, each HTTP method function is used to define a 
different endpoint for our application, and each endpoint has a 
corresponding function in the `taskController` module that handles 
the logic for that endpoint. 

For example, when the `GET /` endpoint is requested, 
the code calls the `getAllTasks()` function in the `taskController` module 
to retrieve all tasks from the database and then sends the result 
as a response to the client.

Similarly, when the `POST /` endpoint is requested, 
the code calls the `createTask()` function in the `taskController` module 
to create a new task in the database using the data sent in the request body.

The `router.delete()`, `router.put()`, and `router.get()` functions work in 
a similar way, each calling a different function in the `taskController` 
module and sending the result back as a response to the client.

Note that some endpoints have a parameter (`:id`) in their URL, 
which allows the client to specify a specific task by its ID. 
The `req.params` object is used to extract this ID from the URL 
and pass it to the appropriate function in the `taskController` module.

Finally, the module exports the `router` instance so that 
it can be used in other parts of the application.




The `router.method()` function is a request handler for a specific route. 
It takes two parameters:
1. `path` (required): the route path for which the handler 
function will be called. It can be a string representing a single path, 
or a regular expression representing a pattern of paths.

2. `handler` (required): the function that will be called 
when a  request is made to the specified route. 
The function takes two parameters:

   - `req`: the request object representing the incoming HTTP request.
   
   - `res`: the response object representing the outgoing HTTP response.

   The handler function should perform any necessary operations, 
   such as retrieving data from a database or rendering a view, 
   and then send the response back to the client using 
   methods on the `res` object. For example, the `res.send()` method 
   can be used to send a string or object as the response body.

[.then() method]

The `.then()` method is a method available on promises in JavaScript. 
When a promise is resolved, it returns a value, which can be accessed 
using the `.then()` method. The `.then()` method is used to 
handle the success or fulfillment of a promise. 

The syntax for using `.then()` is:

```
myPromise.then(onFulfilled, onRejected)
```

(1)Here, `myPromise` is the promise object, `onFulfilled` is a function 
that will be called when the promise is resolved successfully, 

(2)and `onRejected` is a function that will be called when the 
promise is rejected.

***
The `onFulfilled` function will be called with the value 
that was returned by the promise, 
while the `onRejected` function will be called 
with the reason for the rejection. 
If the `onRejected` function is not provided, any errors will be thrown.

*** That's why there's .then().then() codes
The `.then()` method returns a new promise, which can be used 
to chain together multiple promises.


[.send() method]

The `.send()` method is a method available on the response object 
in Node.js. It is used to send a response back to the client 
with a specified payload (usually JSON data or HTML content).

The syntax for using `.send()` is:

```
res.send(data)
```

Here, `res` is the response object, and `data` is the data that you 
want to send back to the client. The `data` parameter can be a 
string, a JSON object, or an HTML file.

***
The `.send()` method "automatically" sets the `Content-Type` header 
based on the type of data being sent. 

For example, if you are sending JSON data, 
the `Content-Type` header will be set to `application/json`.

***
Note that the `.send()` method can only be called once per request. 
If you need to send multiple responses for a single request, you can use 
other methods like `.write()` or `.end()`.

[req.params.id]
`req.params` is an object in Node.js that contains properties 
mapped to the named route parameters. Route parameters are named 
URL segments that are used to capture the values specified at their 
position in the URL. For example, in the route `/users/:userId`, 
`userId` is a route parameter.

****
When a request is made to a route that contains named parameters, 
the values of these parameters are automatically extracted from the 
URL and stored in the `req.params` object.
****

For example, in the following route:

```
router.get("/users/:userId", (req, res) => {
  const userId = req.params.userId;
  // Do something with userId
});
```

If a request is made to `/users/123`, `req.params.userId` would be `"123"`. 

So, in the code `req.params.id` is used to get the value of the `id` 
parameter from the URL path of the request. 
For example, if the URL path is `/tasks/123`, 
`req.params.id` would be `"123"`.

****
The req.params object is useful when you want to retrieve dynamic parts 
of a URL, such as IDs or other parameters. It allows you to access 
the values of these parameters in your route handler and 
use them to perform actions, such as querying a database or rendering 
a template with dynamic content.
*/