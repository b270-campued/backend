// console.log("Hello World!");
// Operators 
// Arithmetic Operators

	let x = 1397;
	let y = 7831;

	let sum	= x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = y / x;
	console.log("Result of division operator: " + quotient);

	let	remainder = y % x;
	console.log("Result of remainder operator: " + remainder);

// Assignment Operators
	
	// Basic Assignment Operator (=)
	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// assignmentNumber = assignmentNumber + 2
	// console.log("Result of addition assignment operator: " + assignmentNumber);

	// Shortcut or Shorthand Operator
	assignmentNumber+=2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	assignmentNumber-=2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber*=2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber/=2;
	console.log("Result of division assignment operator: " + assignmentNumber);

// If all operators are present in one variable? precedence rule on operators
// Multiple Operators 

	// The operations were done in the following order:(MDAS or PEMDAS)
	// 1. Parenthesis
	// 2. Exponent
	// 3. Multiplication
	// 4. Division
	// 5. Addition
	// 6. Subtraction

let number = 1 + 2 - 3 * 4 / 5;
console.log("Result of operator: " + number);

let pemdas = 1 + ( 2 - 3 ) * ( 4 / 5 ); 
console.log("Result of pemdas: " + pemdas)


// Type Coercion

let numA = "10";
let numB = 12;

let coercion = numA + numB; 
console.log(coercion)
// typeof check the data type of the variable
console.log(typeof coercion) 


let numC = 16;
let numD = 14;
let noCoercion = numC + numD;
console.log(noCoercion)
console.log(typeof noCoercion)

let numE = true + 1;
// True is associated to value of 1
console.log(numE)
console.log(typeof numE)

let numF = false + 1;
// False is associated with the value of 0
console.log(numF)
console.log(typeof numF)

// Comparison Operators

let juan = "juan"

// Equality Operator (==) - check whether left and right is the same content and returns boolean value
// EO is not affected by data types

console.log(1==1); //true
console.log(1==2); //false
console.log(1 == "1"); //true EO is not affected by data types
console.log(0 == false) //true
console.log("juan" == "juan") //true
console.log("juan" == juan) //true

// Inequallity Operator

console.log(1!=1); //false
console.log(1!=2); //true
console.log(1 != "1"); //false
console.log(0 != false) //false
console.log("juan" != "juan") //false
console.log("juan" != juan) //false

// Strict Equality Operator - here data type is also considered

console.log(1===1); //true
console.log(1===2); //false
console.log(1 === "1"); //false
console.log(0 === false) //false
console.log("juan" === "juan") //true
console.log("juan" === juan) //true

// Strict Inequality Operator - here data type is also considered

console.log(1!==1); //false
console.log(1!==2); //true
console.log(1 !== "1"); //true
console.log(0 !== false) //true
console.log("juan" !== "juan") //false
console.log("juan" !== juan) //false

//Relational Operators check if value is greater than or less than value

let a = 50;
let b = 65;

// Greater than Operator (>)
let isGreaterThan = a > b;
console.log(isGreaterThan); //false

// Less than Operator (<)
let isLessThan = a < b;
console.log(isLessThan); //true

let isGtOrEqual = a >= b;
console.log(isGtOrEqual); //false

let isLtOrEqual = a<= b;
console.log(isLtOrEqual); //true

// Logical Operators

	let isLegaAge = true;
	let isRegistered = false;

	// Logical AND && operator (&&)
	// Return TRUE if all operands are true (both are true)
	let allRequirementsMet = isLegaAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);

	// Logical OR || operator (||)
	// Return TRUE if atleast ONE of the operands IS true (requires only one is true)
	let someRequirementsMet = isLegaAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT Operator (!)
	// Returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);


// Increment and Decrement
	//Operators that add and subtract values by 1 and reassigns the value of variable 
	//where the increment/decrement was applied to

let z = 1;

// The value of z is added by value of 1 before returning the value and storing it in the variable
// Pre-increment (before variable) - increment before return
let increment = ++z //This causes z increase z by 1
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);


// The value of z is returned first and stored in  the variable "increment" then the value of "z" it increased by 1
//Post-increment (After variable) - return first then increment

increment = z++; //value of increment here is reassigned
console.log("Result of pre-increment: " + increment);
// The value of z is 2 before it was incremented
// z here is already at 2 due to ++z above pre-increment
console.log("Result of pre-increment: " + z);

// Pre-decrement 
// z here is 3 already from above
let decrement = --z
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-decrement 
decrement = z-- //value of decrement here is reassigned
console.log("Result of pre-decrement: " + decrement);
// The value of z is 2 before it was decremented
// z here is already at 2 due to ++z above decrement
console.log("Result of pre-decrement: " + z);