// console.log("Happy Friday!");

// What are conditional statements?
// Conditional Statements allow us to control the flow of our program.
// They also allow us to run statement or instruction if a condition is met.
// Or run another separate instruction if otherwise.

// [SECTION] if, else if and else statement

let numA = -1;

// if statement - executes if specified condition is true.
// Syntax:
/*
	 if(condition) {
		statement
	}

*/
if (numA < 1) {
	console.log("Hello!")
}

console.log(numA < 1); //Remember this returns boolean

let city = "NYC";

if (city === "NYC"){
	console.log("Welcome to NYC!");
} else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!")
}

// else if statement
// Executes a statement if previous conditions are false and if the specified conditio is true

let numH = 1;

if (numH < 0 ){
	console.log("Hello again!");
} else if (numH > 0) {
	console.log("World");
}

// else statement
// Last resort. else if and else can't stand alone like if.

if (numA > 0){
	console.log("Hello")
} else if( numA === 0) {
	console.log("World")
} else {
	console.log("Again")
}


// if, else if and else statements with functions

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30) {
		return "Not a typhoon yet";

	} else if (windSpeed <= 61) {
		return "Tropical depression detected";

	} else if (windSpeed >= 62 && windSpeed <=88) {
		return "Tropical storm detected";

	} else if (windSpeed >=89 || windSpeed <= 117) {
		return "Severe tropical storm detected";

	} else {
		return "Typhoon detected";
	}
}

let message = determineTyphoonIntensity(110);
console.log(message);

if (message == "Severe tropical storm detected") {
	console.warn(message)
}

// [SECTION ]Truthy and Falsy
// Truthy is a value that is considered true when encounted in boolen context
	// Falsy values "", null, NaN

// Truthy examples
if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}

if ([]) {
	console.log("Truthy");
}

// Falsy examples
if (false) {
	console.log("Falsy");
}

if (0) {
	console.log("Falsy");
}

if (undefined) {
	console.log("Falsy");
}
// [SECTION] Conditional Ternary Operator 
// Conditional Ternary operator takes in three operand 
	// 1. condition
	// 2. expression to be executed if the condition is truthy 
	// 3. expression to be executed if the condition is false
	// Syntax : (expression) ? ifTrue : ifFalse;

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

let name;
function isOfLegalAge() {
	name = "John"; //This serve as reassignment of name variable 
	return "You are legel age limit.";
}

function isUnderAge() {
	name = "Jane"; //This serve as reassignment of name variable
	return "You are under the age limit"
}

// "parseInt()" converts the input received from the prompt into a number data type
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 17) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in function: " + legalAge + ", " + name )

// [SECTION] Switch Statement 
	// Syntax: 
	/* switch (expression) {
	   case value :
	   		statement;
	   		break;
	   	default:
	   		statement;
	}
	*/

let day = prompt("What day of the week is it today?").toLowerCase(); //convert input to lowercase
console.log(day);

switch(day) {
	case "monday":
	console.log("The color of the day is red.");
	break; //This causes break the loop if one is already met

	case "tuesday":
	console.log("The color of the day is orange.")
	break;

	case "wednesday":
	console.log("The color of the day is yellow.")
	break;

	case "thursday":
	console.log("The color of the day is green.")
	break;

	case "friday":
	console.log("The color of the day is blue.")
	break;

	case "saturday":
	console.log("The color of the day is indigo.")
	break;

	case "sunday":
	console.log("The color of the day is violet.")
	break;

	default: //incase didn't input valid day
	console.log("Please input a valid day.")

}

// [SECTION] Try-Catch-Finally used for error handly commonly as we want to catch our error
// so that other code still run

function showIntensityAlert(windSpeed){

	try {
		alerat(determineTyphoonIntensity(windSpeed)); //Intended error
	}
	catch (error) {
		console.log(typeof error); //data type of error
		console.warn(error.message); //access property of error
		console.warn(error.name);
	}
	finally {
		alert("Intesity updates will show new alert");
	}
}
showIntensityAlert(56);
