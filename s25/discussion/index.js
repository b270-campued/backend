// console.log("Hi JSON!")

// [SECTION] JSON Objects

/*
	-JSON stands for JavaScript Object Notation
	-JSON is also used in other programming languages
	-Used for serializing different data types into bytes
	-Seralization is the process of converting data into a series of bytes for
	easier transmission/transfer of information
	-SYNTAX:
		{
			"PropertyA": valueA
			"PropertyB": valueB 
		
		}
*/

//JSON Objects

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Phlippines"
// }

// JSON Arrays - Arrays of Data Objects

// "cities": [
// 	{
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Phlippines"
// 	},
// 	{
// 		"city": "Manila City",
// 		"province": "Metro Manila",
// 		"country": "Phlippines"
// 	},
// 	{
// 		"city": "Makati City",
// 		"province": "Metro Manila",
// 		"country": "Phlippines"
// 	}
// ]

// [SECTION] JSON Methods
// The JSON object contains methods for parsing and converting data into stringified JSON

// [SECTION] Converting Array into Stringified JSON
//Take note this is not a JSON thats why batchName does not have ""
let batchesArr = [
	{
		batchName: "Batch 270"
	},
	{
		batchName: "Batch 271"
	}
]

console.log(batchesArr);
//The stringify method is used to convert JS objects into string
console.log("Result of stringify method: ")
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Phlippines"
	}
});

console.log(data);

//User details

let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("How old are you?");

let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city belong to?")
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);


// [SECTION] Converting Stringified JSON into JS Objects

let batchesJSON = `[
	{
		"batchName": "Batch 270"
	},
	{
		"batchName": "Batch 271"
	}
]`;

console.log("Result from parse method: ");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "John",
	"age": 31,
	"address": {
			"city": "Manila",
			"country": "Phlippines"
	}
}`

console.log(JSON.parse(stringifiedObject));

