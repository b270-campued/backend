// console.log("Happy Monday!")

// [SECTION] While loop
	// A while loop takes in an expression/condition
	// If the condition evaluates true, the statement/code block inside executes
/* Syntax:
	while(experession/condition) {
		statment;
	}
*/

// let count = 5 


// // While the value of count is not equal 0 execute console.log and count--
// while(count !== 0) {

// 	// Current value of count is printed
// 	console.log("While: " + count)

// 	// Decrease the value of count by 1 after every iteration
// 	// Iteration is the term given to repetition of statements
// 	count--;
// }

// [SECTION] Do-while loop
	// Do-while works like while loop but unlike the while loop. Do-while loop guarante that the code will be executed once.
/*
	Syntax:

	do {
		statement/codeblock
	} while(expression/condition)
*/

/* How does do -while loop work:

	1. The statement in the do block executes once
	2. The message DO While + number will be printed out in the console
	3. After executing once the while statement will evaluate whenever to run the next iteration of the loop based on given condition.
	4. If condition is met (true), another iteration of the loop will be executed
	5.  If condition is not met (false), loop will stop

*/

// let number = Number(prompt("Give me a number."))

// do {
// 	console.log("Do While: " + number);

// 	// Increases the value of number by 1 after every iteration to stop the loop when it reaches  10 or greater
// 	number+=1;
// }while (number < 10)


// While vs Do-While
// While - evaluate then do
// Do-while - execute then evaluate


// [SECTION] For loop 
	//  A for loop is more flexible than while and do-while loop. It consist of 3 parts:
	// 1. Initialization value that will track up progress of the loop
	// 2. expression/condition that will evaluate if the loop will run one more time
	// 3. finalExpression indicates how to advance the loop

/*

	for(initialization; expression/condition; finalExpression) {
		statement
	}

*/

// for (let count = 0; count<= 2; count++) {
// 		console.log(count);
// }

for (let count = 50; count != -1; count--) {
		console.log(count);
}

let myString = "alex" //space is counted as characters
// Characters in string may be counted using .length property
console.log(myString.length);
// Accessing elements of string using its index number
// Accessed letter a = which is 0 (remember count starts with 0)
console.log(myString[0])
console.log(myString[1])
console.log(myString[2])
console.log(myString[3])

// Will create a loop that will print out the individual letters of myString variable
// Remember mauuna muna yung console.log before x++ kaya naread pa yung index 0 (check while structure)
for (let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}

let myName = "AlEx";

// Creates a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel.
/*
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "i"
        2. It will check if "i" is less than the length of myName (e.g. 0)
        3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
        4. If the expression/condition is true the console will print the number 3.
        5. If the letter is not a vowel the console will print the letter
        6. The value of "i" will be incremented by 1 (e.g. i = 1)
        7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false

*/
for ( let i = 0; i < myName.length; i++) {

    if (
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u"
    ) {
        // If the letter in the name is a vowel, it will print the number 3
        console.log(3);
        
    } else {
        // Print in the console all non-vowel characters in the name
        console.log(myName[i])
    }
}

// [SECTION] Continue and Break Statements

for ( let count = 0; count <= 20; count++) {

	//If remainder is equal to 0
	if (count % 2 ===0) {
		//Tells the code to continue to the next iteration of the loop
		//Also ignores all statements located after the continue statement
		continue;
	}
	console.log("Continue and Break: " + count); //This executes if above statement is false

	// If the current value of count is greater than 10
	if (count > 10) {

		// Tells the code to terminate the loop even if the condition of the loops defines that it should execute so long
		// as the value of count is less than or equal to 20
		break;
	}

}

let name = "alexandro";

for (let i = 0; i < name.length; i++) {

	console.log(name[i])
	
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration.")
		continue
	}

	if(name[i] === "d") {
		break;
	}
}