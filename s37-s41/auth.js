// npm install jsonwebtoken

const jwt = require("jsonwebtoken")

// User defined string data that will be used to create our JSON web token
// Used in the algorithm for encrypting our data which make difficult to decode the 
// information without the defined secret code
const secret = "CourseBookingAPI"

// [Section] JSON Web Tokens
/*

- JSON Web Token or JWT is a way of securely passing information from the server to the frontend 
  or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code can decode the encrypted information
- Imagine JWT as a gift wrapping service that secures the gift with a lock
- Only the person who knows the secret code can open the lock
- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
- This ensures that the data is secure from the sender to the receiver

*/

// Token creation
// Analogy : Pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) => {

	// The data will be received from the registration form (payload)
	// When the user logs in, a token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// Generate a JWT using the jwt's sign method 
	// Token generated here will be use to access parts of application
	// Authorization  is the process of determining user access in the application
	// REST APIs are stateless, hence authorization has to be implemented through JWT
	/*
		Syntax:
		jwt.sign(payload, secretCodeVariable, {options/callBackFunctions e.g validity of token})
	*/
	return jwt.sign(data, secret, {});
}

// Retrieval of Authenticated User's Details

/*
	Workflow:
	1. User logs in, server will respond with JWT on successful authentication
	2. JWT will be included in the header of the GET request to the user/details endpoint
	3. Server validates JWT
		a. If valid - user details will be sent in the response
		b. if invalid - deny request
*/

// Token verification
/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is 
	legitimate and the gift was not tampered with

*/

module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token

	//this is the token copied and pasted in postman
	let token = req.headers.authorization 

	// Token received and is not undefined
	if (token !== undefined) {
		console.log(token);

		// If you check in console there's a Bearer in console.log(token) we use slice it by
		// 7 characters B-E-A-R-E-R-(space)
		// Here we just technically retrive the token without the BEARER+space
		// The "slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "Bearer" token which when received contains the word "Bearer " as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification

		token = token.slice(7, token.length);

		// Verification of token
		// Validates the token using the "verify" method decrypting the token using the secret code
		// jwt.verify(token, secretOrPrivateKey, [options/callBackFunction])

		return jwt.verify(token, secret, (err, data) => {

			// If JWT is not valid
			if (err) {
				// response here is considered object
				return res.send({auth: "failed"});

			// If JWT is valid
			} else {

				// This means that if vefication is success we go 
				// to next function (getProfile)
				// Allows the application to proceed with the next middleware function/callback function in the route
				// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
				next();
			}
		})

	// Token does not exist
	} else {
		return res.send({auth: "failed"});
	}
}

// Token Decryption
// Analogy: Open the gift and get the content

module.exports.decode = (token) => {
	console.log(token);

	// Token recieved and is not undefined
	if (token !== undefined) {

		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null;

			} else {

				//We only access payload here
				// The "decode" method is used to obtain the information from the JWT
				// When the complete option is set to true, the jwt.decode() method will decode the complete JWT and return an object that includes the JWT header, payload, and signature.
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				// Syntax: jwt.decode(token, [options])
				return jwt.decode(token, {complete:true}).payload;
			}

		})
	} else {

		return null;
	}
}
