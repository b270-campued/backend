// Minimum Viable Products
/*
	

A Minimum Viable Product (MVP) is a version of a product with just enough features to 
satisfy early customers and provide feedback for future product development. 
The goal of an MVP is to quickly test assumptions about the product and gather data to 
inform further development.

Here are some characteristics of a minimum viable product:
1. It should have enough features to solve a specific problem or address a particular need.
2. It should be easy to use and understand for early customers.
3. It should be built quickly and efficiently with minimal resources.
4. It should provide valuable feedback to inform future product development.
5. It should be able to generate interest and excitement from potential customers.

Examples of MVPs could include a simple mobile app with a few key features, 
a prototype of a physical product, or a landing page with a sign-up form to gauge 
interest in a new service.

The key to creating a successful MVP is to focus on the core features that will solve a 
specific problem for early customers and gather feedback to inform further development. 
By creating an MVP, you can test assumptions and validate your product idea 
before investing significant time and resources into building a fully-featured product.
*/

// Booking Systems MVP Requirements

/*
	Booking System MVP must have the following features:
    - User registration
    - User authentication (login)
    - Retrieval of authenticated user's details
    - Course creation by an authenticated user
    - Retrieval of courses
    - Courses info updating by authenticated user
    - Course archiving by an aunthenticated user


*/

// Other Depencies besides express and mongoose packages

/*
 - bcryp (hashing user password prior to storage)
 - cors (allowwing cross-origin resource sharing)
 - jsonwevtoken (implementing json web tokens)

*/

// npm init -y
// npm install express
// .gitignore > node_modules
// npm install mongoose

// If you want to clone projects node_modules is ignored. So for it to work used the terminal and type : npm install


const express = require("express");
const mongoose = require("mongoose");

//Allows our backend application to be available to our frontend
const cors = require("cors")

//Allow access to routes defined within the application
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

const app = express();

// Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.zmpg8k6.mongodb.net/b270_booking?retryWrites=true&w=majority", 
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }   
);

// Set notifications for database connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We are connected to cloud database"));


//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Middleware that defines the "/users" to be included for all user routes
app.use("/users", userRoutes);
// Middleware that defines the "/courses" to be included for all course routes
app.use("/courses", courseRoutes);



// Will use the defined port number for the application whenever an environment variable
// is available OR will use port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application 

/*
    This code starts a server that listens for incoming requests on a specified port. 
    The app object is an instance of an Express application.

    The listen() method is used to start the server and bind it to a specific port. 
    In this case, it is listening on either the port specified in the process.env.PORT 
    (environment variable) or port 4000 if the environment variable is not defined.


    ***The app.listen() method is called with two arguments: the port number to listen on, 
    and a callback function to execute once the server is running.

    The first argument is process.env.PORT || 4000. This means that the server will listen on 
    the port specified in the process.env.PORT environment variable, if it exists, or 
    port 4000 if the environment variable is not defined. This allows the server to be deployed to 
    platforms like Heroku, which dynamically assign a port at runtime.

    The second argument is a callback function that logs a message to the console once the server is 
    running. The message includes the port number that the server is listening on, which is either 
    the value of process.env.PORT or 4000 if the environment variable is not defined.

    Once the server is running, it will listen for incoming requests on the specified port, 
    and any routes defined in the application will be available to clients that connect to that port.
*/
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
})

// Install Cors package (cross origin resource sharing)
// npm install cors
/*
    Allows our backend application to be available to our frontend
    To make frontend and backend connect.
*/