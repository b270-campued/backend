const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth.js")

// Route for checking if the user's email already exist in the database
// Invokes the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", userController.checkEmailExists);

// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.loginUser);

// Route for checking user details
// This is using POST method even its retrieve?
// It's post as we have request body
// router.post("/details", userController.getProfile); refactored by below
// We refactor it to new code/there's changes due to token verification and decoding
/*
Change method to get then remove req body > Then in postman > 
Get a token using user log in request > Copy token > 
Go to Retrieve user request > Authentication tab > Type: Bearer Token > 
Paste the copied token in token input
*/

// upon accessing /details auth.verify will be invoked
// if valid token and verified it will proceed to getProfile
router.get("/details", auth.verify, userController.getProfile)

// Route for enrolling a user to a course
router.post("/enroll", auth.verify, userController.enroll);

module.exports = router;