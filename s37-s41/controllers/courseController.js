const Course = require("../models/Course");
const auth = require("../auth.js")


// Create a new course
/*
	Steps:
	1. Create a conditional statement that will check if the user is an administrator.
	2. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	3. Save the new User to the database
*/

/* Refactor
module.exports.addCourse = (req, res) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		slots: req.body.slots
	});

	return newCourse.save().then(course => {
		console.log(course);
		res.send(true);
	})
	.catch(error => {
		console.log(error);
		res.send(false)
	})
}
*/

/*

module.exports.addCourse = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin !== true ) {

		return res.status(403).send({message: "You are not authorized to perform this action"})

		
	} else {

		let newCourse = new Course(
		{
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		});

		return newCourse.save().then(course => {
			console.log(course);
			res.send(true);
		})
		.catch(error => {
			console.log(error);
			res.send(false)
		}) 

	}
}

*/

module.exports.addCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		});

		return newCourse.save().then(course => {
			console.log(course);
			res.send(true);
		})
	} else {
		return res.send(false)
	}
}

// Retrieve all courses
/*
	Step:
	1. Create a conditional statement that will check if the user is an administrator.
	2. Retrieve all the courses from the database
*/

module.exports.getAllCourses = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {

		return Course.find({}).then(result => res.send(result));

	} else {
		return res.send(false);
	}
}

// Retrieve all active courses 

/*
	Step:
	1. Retrieve all the courses from the database with property active 
*/

module.exports.getAllActive = (req, res) => {

	return Course.find({isActive: true}).then(result => res.send(result));
}

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (req, res) => {
	//courseId -> used as its the parameter in params.courseId
	console.log(req.params.courseId);
	return Course.findById(req.params.courseId).then(result => {
		return res.send(result)
	})
	.catch(error => {
		return res.send(error);
	})

}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}

		/*
			Syntax:
			findByIdAndUpdate(documentId, update)




		`findByIdAndUpdate()` is a method in Mongoose, an Object Data Modeling (ODM) library for 
		MongoDB and Node.js. It is used to find a document by its unique `_id` field and update it. 
		The parameters for `findByIdAndUpdate()` are as follows:

		1. `id`: The unique identifier of the document to be updated.
		2. `update`: An object containing the properties to be updated and their new values. 
		This object can also contain update operators such as `$set`, `$inc`, `$push`, etc.
		3. `options`: An optional object that can be used to specify various options for the update 
		operation. Some of the commonly used options are:

		   * `new`: A boolean value that specifies whether to return the updated document or 
		   the original document before the update.
		   * `upsert`: A boolean value that specifies whether to insert a new document if no 
		   document matches the `id` parameter.
		   * `runValidators`: A boolean value that specifies whether to run Mongoose validators 
		   when executing the update operation.

		4. `callback`: An optional function that is called after the update operation completes. 
		The callback function has two parameters: `error` and `doc`. If an error occurs during the 
		update operation, `error` will be populated with the error message, and `doc` will be `null`. 
		If the update operation is successful, `error` will be `null`, and `doc` will contain the 
		updated document (depending on the `new` option).

		Alternatively, you can also use `findByIdAndUpdate()` with a promise instead of a 
		callback function by omitting the `callback` parameter.
				
		*/

		// You can add optional {new: true} in parameters of findByIdAndUpdate so that 
		// result will be the updated
		/*
			Syntax:
			findByIdAndUpdate(documentId, update, {new:true})
		*/
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse).then(result => {
			console.log(result);
			res.send(result)
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
		// return res.send("You don't have access to this page!");
	}
}

// ACTIVITY Archive Course

/*
	Steps:
	 1. Create a route for archiving a course. The route must use JWT authentication and 
	 	obtain the course ID from the url.
     2. Create a controller method for archiving a course obtaining the course ID from 
        the request params and the course information from the request body.
*/

// Archive a course / Unarchiving
// In managing databases, it's common practice to soft delete our records 
// and what we would implement in the "delete" operation of our application

// The "soft delete" happens here by simply updating the course "isActive" 
// status into "false" which will no longer be displayed in the frontend 
// application whenever all active courses are retrieved

// This allows us access to these records for future use and hides them 
// away from users in our frontend application

// There are instances where hard deleting records is required to maintain 
// the records and clean our databases

// The use of "hard delete" refers to removing records from our 
// database permanently

module.exports.archiveCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {

		let archiveCourse = {
			isActive: req.body.isActive
		}

		return Course.findByIdAndUpdate(req.params.courseId, archiveCourse, {new:true}).then(result => {
			console.log(result);
			// Course status is successfully updated
			res.send(true)
		})
		.catch(error => {
			console.log(error);
			// Course update failed
			res.send(false);
		})
	} else {
			// Action not allowed to non-admin
			return res.send(false);
		// return res.send("You don't have access to this page!");
	}
}
