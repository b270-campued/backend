const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")

// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) => {
	/*
		It expects to receive the email address to check in the request body (req.body.email).
		It calls the find() method on the User model to search for any records that have a 
		matching email address.
	*/
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		/*
			If a matching record is found (i.e. result.length is greater than zero), 
			the function returns true, indicating that the email address is already in use.
		*/
		if (result.length > 0) {
			return res.send(true);

		//No duplicate email found
		/*
			If no matching record is found, the function returns false, 
			indicating that the email address is available for use.
		*/
		// The user is not yet registered in the database
		} else {
			return res.send(false)
		}
	})
	/*If an error occurs during the database query, 
	the function returns the error message to the client.*/
	.catch(error => res.send(error))

	/*
		This function could be used, for example, to validate a user's email address during 
		registration to ensure that it hasn't already been used by another user.
	*/

	/*
		The `.catch()` method is a built-in method in JavaScript that can be called on \
		a promise object. It takes a function as an argument that will be called if 
		the promise is rejected (i.e. an error occurs).

		In the code  provided, the `.catch()` method is being used to handle any errors 
		that might occur during the database query. If an error occurs, the function will 
		return the error message to the client using the `res.send()` method.
	*/
}

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	// Encryption of information package
	// npm install bcrypt

	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		/*
			Syntax:
			bcrypt.hashSync(dataToBeEncrypted, saltRounds)

			bcrypt.hashSync() is a synchronous method in the bcrypt library that is used to 
			hash a password. Hashing is a process of converting a plain-text password into a 
			fixed-length, irreversible string of characters that cannot be decrypted. 
			This is a common technique used to store passwords securely in a database, 
			as it allows the password to be compared without the need to store the original 
			plain-text password.

			The bcrypt.hashSync() method returns a string representing the hashed password, 
			which can be stored in a database. Later on, when a user attempts to log in, 
			their entered password can be hashed using the same algorithm, and compared to the 
			stored hashed password to determine if they are correct.

			***It's worth noting that bcrypt.hashSync() is a synchronous method, 
			which means it will block the Node.js event loop until the hashing process is complete. 
			This can be a performance issue in some cases, so it's recommended to use the 
			asynchronous version, bcrypt.hash(), in production code.

			bcrypt.hashSync() accepts two parameters:

			password (required): A string representing the plain-text password to be hashed. 
			This is the value that will be converted into a fixed-length, irreversible string of 
			characters that cannot be decrypted.

			saltRounds (optional): An integer representing the number of salt rounds to use in the 
			hashing algorithm. The salt rounds determine the complexity and security of the hash. 
			The higher the number of salt rounds, the more secure the hash will be, 
			but the longer it will take to compute. The default value for saltRounds is 10.
		*/
		// 10 is the value provided as the number of "salt" rounds that the bcrypt
		// algorithm will run in order to encrypt 
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Save the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		//User does not exist 
		if(result == null) {
			return res.send({message: "No user found"})

		//User exist
		} else {

			/*
			Creates the variable "isPasswordCorrect" to return the result of comparing the login form 
			password and the database password.

			The "compareSync" method is used to compare a non encrypted password from the login form 
			to the encrypted password retrieved from the database and returns "true" or "false" value 
			depending on the result

			*/

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			// isPasswordCorrect now here either contain true or false
			if (isPasswordCorrect) {

				// Generate an access token by invoking the "createAccessToken" in the auth.js file
				return res.send({accessToken: auth.createAccessToken(result)});
			
			// Passwords do not match
			} else {
				return res.send(false)
			}
		}
	})
}

// [ACTIVITY]

// Retrieve user details

/*
	Steps:
	1. Find the document in the database using the user's ID
    2. Reassign the password of the returned document to an empty string
    3. Return the result back to the postman
*/

/*
module.exports.getProfile = (req, res) => {

	return User.findById({_id: req.body.id}).then(result => {
		
		if (result === null) {
			return res.send({message: "User not found"});
		} else {
			result.password = "";
			return res.send(result);
		}
	})
	.catch(error => res.send(error))
}
*/

module.exports.getProfile = (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to 
	// retrieve the user information from the token passing the "token" 
	// from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	/*let input = req.body;*/ // Refactored
	// We refactor it to new code/there's changes due to token verification and decoding
	// Change method to get then remove req body

	/*
	Change method to get then remove req body > Then in postman > 
	Get a token using user log in request > Copy token > 
	Go to Retrieve user request > Authentication tab > Type: Bearer Token > 
	Paste the copied token in token input
	*/

	return User.findById(userData.id).then(result => {

		/*
		Changes the value of the user's password to an empty string when 
		returned to the frontend.

		Not doing so will expose the user's password which will also not 
		be needed in other parts of our application

		Unlike in the "register" method, we do not need to call the 
		mongoose "save" method on the model because we will not 
		be changing the password of the user in the database but only 
		the information that we will be sending back to the frontend application
		
		*/

		result.password = "";

		// Returns the user information with the password as an empty string
		return res.send(result);

	})

}


/*
	The findById() method is a mongoose method used to find a document by its _id field. 
	The _id field is a unique identifier assigned to every document in a MongoDB collection.

	The findById() method takes one parameter, which is the value of the _id field of the 
	document to be found. The parameter can be a string representing the hexadecimal 
	value of the _id field, or a variable that holds such a string.

	The parameter can be passed to the method as a plain object with an _id property.

	***The method returns a query object that can be further modified using other methods provided by 
	mongoose, such as .select(), .populate(), and so on. The query object is also a promise, 
	so it can be handled using .then() and .catch() methods.
*/

/*Workflow:

1. User logs in, server will respond with JWT on successful authentication
2. A POST request with the JWT in its header and the course ID in its request body will be sent to the /users/enroll endpoint
3. Server validates JWT
4. If valid, user will be enrolled in the course
5. If invalid, deny request

*/

// Enroll user to a course
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Database

	Workflow:
	1. User logs in, server will respond with JWT on successful authentication
	2. A POST request with the JWT in its header and the course ID in its request body will be sent to the /users/enroll endpoint
	3. Server validates JWT
		- If valid, user will be enrolled in the course
		- If invalid, deny request
*/

module.exports.enroll = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin) {
		// To retrieve the course name
		let courseName = await Course.findById(req.body.courseId)
		.then(result => result.name);


		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			// Course ID will be retrieved from the request body
			courseId: req.body.courseId,
			courseName: courseName
		}
		console.log(data);


		// Add the course ID in the enrollments array of the user
		// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
		// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend

		let isUserUpdated = await User.findById(data.userId).then(user => {

			// Adds the courseId in the user's enrollments array
			user.enrollments.push({
				courseId: data.courseId,
				
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);


		// Add the user ID in the enrollees array of the course
		// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend

		let isCourseUpdated = await Course.findById(data.courseId)
		.then(course => {

			// Adds the userId in the course's enrollees array
			course.enrollees.push({
				userId: data.userId
			})
		
			// Minus the slots available by 1
			course.slots -= 1;

			// Saves the updated course information in the database
			return course.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isCourseUpdated);


		// Condition that will check if the user and course documents have been updated

		// User enrollment successful
		if(isUserUpdated && isCourseUpdated) {
			return res.send(true);

		// User enrollment failure
		} else {
			return res.send(false);
		}
	} else {
		return res.send(false);
	}
}