const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password  is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	createdOn: {
		type: Date,

		// The "new Date()" expression instantiates a new "date"
		// that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required"]
			},
			status: {
				type: String,
				default: "Enrolled"
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);