// console.log("Hello World!")


// Array Methods 
// JS has built i functions and methods for arrays which allow us to manipulate and access array items.

//mutation seek to modify contents of array
//iteration aim to evaluate each element in array

// [SECTION] Mutator Methods

/*
	These are methods that mutate or change an array after they are created.
	These methods manipulate the original array performing various task such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
	//Adds element/s at the end of an array AND returns the added element
	// arrayName.push(elementToBeAdded);

console.log("Current fruits array:");
console.log(fruits);


let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Adding Multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
	//Removes element/s at the end of an array AND returns the removed element
	//arrayName.pop();

let removedFruit = fruits.pop();
console.log(removedFruit);

console.log("Mutated array from pop method:");
console.log(fruits);

// unshift()
	//Adds element/s at the beginning of an array AND returns the new array
	//arrayName.unshift(elementA, ElementB)

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);

//shift()
	//Removes an element at the beginning of an array AND returns the removed element
	//arrayName.shift();

let anotherFruit = fruits.shift();
console.log(anotherFruit); 
console.log("Mutated array from shift method:");
console.log(fruits);

// splice()
	//simultaneously removes elements from a specified index number and adds elements
	// arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

let fruitSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitSplice);
console.log("Mutated array from splice method:");
console.log(fruits);

// sort() 
	//Sort elements in alphabetical order
	// arrayName.sort();

let fruitSort = fruits.sort();
console.log(fruitSort);
console.log("Mutated array from sort method:");
console.log(fruits);

//reverse()
	//Reverses the order of array elements
	//arrayName.reverse();

let fruitReverse = fruits.reverse();
console.log(fruitReverse);
console.log("Mutated array from reverse method:");
console.log(fruits);

//Non-Mutator Methods
/*
	Methods that do not modify or change an array after there are created
	Methods do not manipulate the original array
	Perform various task such as returning elements from an arrat and combining arrays and printing outputs
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

//indexOf()

	//Returns the index number of the first matching element found in an array
	// The search process will start from the first element proceeding to the last element
	//If no match found it will return -1
	//arrayName.indexOf(searchValue);

let firstIndex = countries.indexOf("PH", 2); //2 here means it will start at index 2 that's why this returns 5 and also applies in lastIndexOf
console.log("Result of indexOf method: " + firstIndex); //search is from beginning and first one to check is index 1 eventhough ph is also in index 5

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry); //If no match found it will return -1

// lastIndexOf()
	//Returns the index number of the last matching element found in an array
	//The search process will start from the last element proceeding to the first element
	//If no match found it will return -1
	//arrayName.lastIndexOf(searchValue);

let lastIndex = countries.lastIndexOf("PH", 4); //4 here means it will start at index 4 that's why this returns 1
console.log("Result of lastIndexOf Method: " + lastIndex);

//slice 
	// Portions or slices elements from an array and returns the new sliced array
	//arrayName.slice(startingIndex);

let sliceArrayA = countries.slice(2);
console.log("Result of slice method: ");
console.log(sliceArrayA); // ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE'] slice orig array starting from index 2 (means returning)

let sliceArrayB = countries.slice(2, 5);
console.log("Result of slice method: ");
console.log(sliceArrayB); // ['CAN', 'SG', 'TH'] Slicing off specific end to end point means index 2 to BEFORE 5 will be returned

let sliceArrayC = countries.slice(-3);
console.log("Result of slice method: ");
console.log(sliceArrayC); // ['PH', 'FR', 'DE'] Slicing off elements starting last element


// toString()
	// Return an array as a string separated by commas

let stringArray = countries.toString();
console.log("Result of toString method: ");
console.log(stringArray); // 

// concat()
	// Combines arrays
	// arrayName.concat(arrayB)

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "bake bootstrap"];
let taskArrayC = ["git", "node"];

let task = taskArrayA.concat(taskArrayB);
console.log("Result of concat method: ");
console.log(task);

//Combining multiple arrays 
console.log("Result from concat method:");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

let combinedTask = taskArrayA.concat("express", "react");
console.log("Result from concat method:");
console.log(combinedTask);

// join()
	// Returns an array as a string separated by specified separator
	// arrayName.join("separator")

let users = ["John", "Jane", "Joe", "Joshua"];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));

// [SECTION] Iteration Methods
/*
	

*/

// forEach()
	//Similar to for loop that iterates on each array element
	//For each item in the array the anonymous function passed in the forEach()
	//will be run
	//The anonymous function is able to receive the current item being iterated or looped
	//over by assigning a parameter (in the ex. task)
	//Does not return anything

	/* arrayName.forEach(function(indivElement) {
			statement;
	   })

	*/

let filteredTask = [];

allTasks.forEach( function(task) {
	if (task.length > 10) {
		filteredTask.push(task);
	}
})

console.log("Result of forEach method: ")
console.log(filteredTask);

// map()
	// Iterates on each element and RETURNS a new array.
	/*	arrayName.map(function(indivElement) {
			statement
	  	})
	*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number;
})

console.log("Original Array: ")
console.log(numbers);
console.log("Result of map method: ")
console.log(numberMap)

// map() - return new array vs forEach() - does not return value

let numberForEach = numbers.forEach(function(number) {
	return number * number
})

console.log(numberForEach)

// every()
	// check if all elements in an array meet the given condition
	// Returns true if all elements meet the conditions and false if otherwise
	// Returns boolean value (AND &&)

let allValid = numbers.every(function(number) {
	return number < 3;
})

console.log("Result of every method: ");
console.log(allValid);

//some()
	//Checks if atleast one element in an array meets the given condition
	// Returns true if some elements meet the conditions and false if otherwise
	// Returns boolean value (OR ||)

let someValid = numbers.some(function(number) {
	return (number < 2);

	
	return (number < 2);
})

console.log("Result of some method: ");
console.log(someValid);

// filter()
	//Returns a new array that contains the elements which meet the condition
	//Returns an empty array if no element met the condition

let filterValid = numbers.filter( function(number) {
	return (number < 3);
})

console.log("Result of filter method: ");
console.log(filterValid);


// includes()
	//Checks if the argument passed can be found in the array
	// Returns boolean value

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

//reduce()
	// It evaluates elements from left to right and returns/reduces the array into single value

let iteration = 0;
let reducedArray = numbers.reduce(function (x,y) {

	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("Current value: " + y);

	return x + y;

})

console.log("Result of reduce method: " + reducedArray);


let list = ["Hello", "Again", "World"];

let reducedString = list.reduce(function(x,y) {
	return x + " " + y;
})

// First iteration x = Hello , y =  Again => Hello Again
// Second iteration x = Hello Again, y = World => Hello Again World

console.log("Result of reduce method: " + reducedString);
