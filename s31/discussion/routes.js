const http = require("http");

// Create a variable port to store the port number
const port = 4000

// Creates a variable server that stores the output of the "createServer()" method.
const server = http.createServer((request,response) => {

	// dot notation is used here to access properties in an object so it means request and response is object 
	// Accessing the 'greeting' route returns a message of "Hello World!"
	// request.url is checking relative URL. localhost:4000 is the base url
	if (request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello, Batch 270!");
	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("This is the homepage");
	} else if (request.url == '/profile') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("This is the profile page");
	} else {
		// 404 should be status code since page is not found (error status code)
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("Page not available");
	}
});

server.listen(port);
console.log(`Server is now accessible at localhose: ${port}.`)

// terminal - ctrl + c to cancel running server (to stop the server)
// install - terminal : npm install -g nodemon - terminal : nodemon routes.js (to install a package in global --> nodemon package)
// running serve - terminal : nodemon fileName.js
// Using nodemon no need to cancel the running server for any updates to run its automatic