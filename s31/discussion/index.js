// NODE.JS Introduction
// Server side programming (CLIENT SERVER ARCHITECTURE)

/*At this point we know how to:
1. Develop a static front-end application (Capstone One)
2. Manipulate Database

What if we need our front end app to show dynamic content? We need to send request to a server
that would turn query and manipulate database for us.

NODE JS is open source and used for server side application (backend side). 
NOD JS is JS runtime environment for creating server-side applications. \

Runtime environment
-Gives the context for running a program language
-JS was initially within context of the browser but with NODE JS the context was taken out of the browser 
and put into server and with NODE JS, JS now has access to system resources, memory, file system, network, etc.
-WE TECHNICALLY NEED NODE JS so we can use JS in the backend (server side)


BENFITS
-Performance (Optimized for web applications)
-Familiarity
-Access to Node Package Manager (NPM) - world's largest registry of packages
*/

// CREATE NODE JS SIMPLE SERVER
// "http" is a NODE JS module that lets NODE JS transfer data (send and receive http request) using Hyper Text Transfer Protocol
// Use the "require" directive to load NODE JS modules
// Module is a software component or part of a program that contains one or more routines or functions
// Provides functionality for creating HTTP servers and clients, allowing applications to send and receive (fetchinh resources) HTTP request.

let http = require("http");

/*Using this module's createServer() method, we can create an HTTP server that listens
to requests on a specified port and gives response back to the client.The method receives a function*/

//The message sent by the client, usually a web browser, are called "request"
// The message sent by the server as an answer is called responses

/*The server will be assigned to port 4000 via "listen(4000) method where the server will listen to any
request that are sent to it*/

/*Ports are virtual places within an operating system where network connections start and end. 
They help computers sort the network traffic they receive.*/

// "200" status code which means "okay"
// Status codes : https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

http.createServer(function (request,response) {

	// Uses the writeHead() method to:
		//set the status code for the response - 200 means OK
		// set the content type of the reponse as a plain text message (there are other content types you can search)
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// Send the response with the text content "Hello World"
	response.end("Hello World!");

}).listen(4000);

// When server is running, console will print the message:
console.log("Server is running at localhost:4000") // This will be printed in terminal and not in web console

// Go to discussion folder - open terminal - type : node fileName.js to run the server