// Introduction to Express.js (A Node.js Framework that is used to create API)

/*
	What is Express.JS (Unopinionated Framework)
	-opensource web application framework written in JS and hosted in Node.js
	-designed to build APIs web applications cross-platform mobile apps quickly and make Node.js easy.

	Web Framework
	-components designed to simplify the web dev process
	-allows devs to focus on the business logic in their app
	-can be opinionated: framework dictates how it should be used by the dev, speeds up the start up process of app development,
	enforce best practices for the frameworks use case, but lack of flexibility could be a drawback when apps needs are not aligned to the 
	frameworks assumptions
	-can be unopinionated: dev dictates how to use the framework, offers flexibility, abundance of options maybe overwhelming

	Advantages over plain Node.js
	-Simplicity makes it easy to learn and use
	-Offers ready to use components
*/

// Initialize node.js packages: Git bash > npm init > (world largest registry of packages) > Enter x 9 > yes > package json file will appear
// Installing express : npm install express

const express = require("express"); // directive here is "express" not "http"

//this creates express application and stores this in a constant called app
//this is our server (unlike node that have createServer methods etc.)
const app = express(); 

const port = 3000;

// This is a middleware that converts data (body text) to JSON (remember front and back app uses text/string to send and receive data middle ware manage the conversion to json as DB such as MongoDB uses JSON format)
// Middleware is a request handler that has access to the applications request-response cycle (there are alot of middlewares)
app.use(express.json());

//Middleware that allows our app to read data from forms (ex. html forms)
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes

// Express has methods coresponding to each HTTP method

// Get route
// This route expects to receive the GET request at the base URI "/"
// This route will return a simple message back to the client
app.get("/",(req, res) => {

	// res.send uses the Express JS module's method to send response back to the client vs writeHead etc. of HTTP
	res.send("Hello World!")
})

// Post route
// This route expect to recieve a POST request at the URI "/hello"
app.post("/hello", (req, res) => {

	// We can use dot notation here as data are converted already to JSON with this code: app.use(express.json()); above
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

//mock database
let users = []; //Empty Array


app.post("/signup", (req, res) => {

	console.log(req.body); // This will show in terminal to test if data input is stored properly

	if (req.body.username !== "" && req.body.password !== "") {
		users.push(req.body);
		res.send(`User ${req.body.username} is succesfully registered!`)
	} else { // if empty or not complete this will run and error message will sent back to client/Postman!
		res.send(`Please input BOTH username and password.`)
	}
})

// This route expects to receive a PUT requests at the URI "/change-password"
app.put("/change-password", (req, res) => {
	let message;

	// Creates a for loop that will lopp through the elements of the "users" array
	for (let i = 0; i < users.length; i++){

		// Changes the password of the user found by the loop into the password provided in the client
		if (req.body.username == users[i].username){ //if input username is equal to username
			users[i].password = req.body.password; //new password from input will be assigned

			message = `User ${req.body.username}'s password has been updated.`;
			break

		// If no user was found
		} else {

			message = `User does not exist.`;
		}		
	}
	res.send(message);
})

// ACTIVITY

// GET HOME ROUTE
app.get("/home",(req, res) => {

	res.send("Welcome to homepage")
})

// GET USERS ROUTE

app.get("/users",(req, res) => {

	res.send(users)
})

// DELETE USER ROUTE

app.delete("/delete-user",(req, res) => {
	let message;

	for (let i = 0; i < users.length; i++){

		if (req.body.username == users[i].username){ 

			message = `User ${req.body.username} has been deleted.`;

			break

		// If no user was found
		} else {

			message = `User does not exist.`;
		}		
	}

	res.send(message);
})


// Remember you need to make routes inside the server so that when you create request in postman they will run
// If no content type speficied in response default is HTML
app.listen(port, () => console.log(`Server is running at port ${port}`));

