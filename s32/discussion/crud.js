let http = require("http");
const port = 4000;

//mock database
// [] - Array {} -Object
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Taylor",
		"email": "tswift@mail.com"
	}
]

const server = http.createServer((request,response) => {
	// checks if the url and method matches the required condition
	if(request.url == "/users" && request.method == "GET") {
		// content-type is set to application/json because in real world
		// management of databases, we are working with objects and less texts.
		/*MongoDB is document based database - data are in form of objects. Content type here is object*/
		
		response.writeHead(200, {"Content-Type": "application/json"});
		
		/* The frontend and backend applications communicate through strings. 
		We have to convert the JSON data type from server into stringified JSON
		when it is sent to frontend application*/
		
		response.write(JSON.stringify(directory)); //To convert into string as front and back comminicate in string form and directory is in object

		// ends the response process
		response.end();


	} else if(request.url == "/users" && request.method == "POST") {

		//Created a requestBody var to an empty string
		let requestBody = '';

	// In Node.js, 'request.on' is a method that is used to register event listeners for events emitted by an HTTP request object.
	 	/*
	 	Takes two arguments:
			- the event name
			- a callback function/event handler to be executed when the event is emitted.
		*/
			
	// When a request with a body is received by the server, Node.js streams the request body in chunks. The 'data' event is emitted for each chunk of data received by the server.

		// The requestBody will go through data stream; a sequence of data
		// Data is received from the client and is processed in the "data stream"
		// 1st step of stream
		// "data" step - data is received from the client and is processed in the "data" stream
		// This reads the "data" stream and processes it as the requestBody
		request.on('data'/*fixed*/, function (data) {

			// Assigns the data retrieved from the data stream to requestBody
			requestBody+= data;
		});
		//2nd step of stream
		// end step - only runs after the request has completely be sent
		request.on('end'/*fixed*/, function(){
			console.log(typeof requestBody); // String
			//Parse - Convert stringified JSON to JS object
			requestBody = JSON.parse(requestBody); // Convert the string to JS Object
		
		let newUser = {
			"name": requestBody.name, //since its converted to JS object we can also use dot notation to get them 
			"email": requestBody.email //since its converted to JS object we can also use dot notation to get them 
		}

		console.log(typeof newUser) 
		// adding the newly created object to the directory/mock database
		directory.push(newUser);
		console.log(newUser);

		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(newUser));
		response.end();
		
		})

	}
});

server.listen(port);

console.log(`Server is live at port ${port}.`)

// http, response, request are directives
// createServer, writeHead, write, end, on are methods

// Using Postman as frontend form substitute
// Postman > click raw > select JSON > type:
/*
	{
	    "name": "john",
	    "email": "john#mail.com"
	}
*/
