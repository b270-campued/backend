// NODE JS ROUTING WITH HTTP METHODS
// Access URL endpoints using HTTP methods

//Managing Routes Review

/*Last discussion is sufficient for retrieving resources from different endpoints.
But what about create, update, and updating?

Commonly used HTTP methods:
GET - Retrieves resources
POST - Sends a data for creating a resource
PUT - Send a data for updating a resource
DELETE - Deletes a specified resource

*/

// POSTMAN CLIENT - Used to improve the experience of sending request, inspecting response etc.
// Used to test request if we don't have frontend

let http = require("http");

const port = 4000;

const server = http.createServer((request,response) => {
	//HTTP Method of incoming request can be accessed via "method"  property in request parameter
	// "GET" means that we will be retrieving/reading information
	if (request.url == "/items" && request.method == "GET") {
		// Request the "/items" path and "GETS" Information
		response.writeHead(200, {"Content-Type": "text/plain"});
		// End the request-response cycle
		response.end("Data retrieved from database")
	} else if (request.url == "/items" && request.method == "POST") {
		// Request the "/items" path and "POST" Information
		response.writeHead(200, {"Content-Type": "text/plain"});
		// End the request-response cycle
		response.end("Data to be sent to the database")
	}

});

server.listen(port);

console.log(`Server is live at port ${port}.`)

// Creating a request 
// Postman > Workspaces > Create New Collection > Add Request > Set Request Name > Choose request method > Type the request URL
// > Click Send > In response panel it will show the response.end content