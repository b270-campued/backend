//CRUD Operations
/*
	-CRUD operations are the heart of any backend application
	-Mastering the CRUD operations of any language makes us a valuable developer
	and makes the work easier for us to deal with huge amount of information
	
*/

// [SECTION] CREATE (Inserting Documents)

/*
	SYNTAX: 
		db.collectionName.insertOne({object}) 

		Insert Many:
		db.collectionName.insertMany([{objectA}, {ObjectB}]) arrays of objects 

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact : {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

//Insert Many

db.users.insertMany([

	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact : {
		phone: "0987654321",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
	},

	{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact : {
		phone: "0987654321",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "SaaS"],
	department: "none"
	}
])

// [SECTION] READ (Finding documents)

/*
	db.users.find(); - Retrive all of the documents (search criteria empty)
	db.users.find({ field: value}); - Specific Retrieval

*/

db.users.find();

//Finding Single Document
db.users.find( {firstName: "Stephen"});
db.users.find( {age: 82});

//Finding documents with multiple parameters
/*
	SYNTAX:
	db.users.find( {fieldA: valueA, fieldB: valueB});

*/
db.users.find( {lastName: "Armstrong", age: 82});

// [SECTION] UPDATE (Updating documents)

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact : {
		phone: "00000000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})

//Updating a single document
/*
	-Just like the "find" method, methids that only manipulate single document
	will only update the FIRST document that mathces the search criteria

	SYNTAX:
	db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/


db.users.updateOne(
	{firstName: "Test"}, 
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);


//Updating multiple documents

/*
	-Syntax: 
	db.collectionName.updateMany({criteria}, {$set: {field:value}})
*/


db.users.updateMany(
	{"department" : "none"}, 
	{
		$set: {department: "HR"}
	}
)

//Replace One will replace whose data set  unlike updateOne and updateMany that sets specific data to be updated
// Can be used if replacing the whole document necessary

db.users.replaceOne(
	{ firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
	}
)

// DELETE (Deleting documents)

//Document to delete
db.users.insertOne(
	{
		firstName: "Test"
	}
)


//Deleting a single document

/*
	SYNTAX:

	db.collectionName.deleteOne({criteria}) - First to find
*/

db.users.deleteOne({firstName: "Test"});


//Deleting many document

/*
	-Be careful when using deleteMany. If no search criteria is provided it will delete all the documents in database
	SYNTAX:
	db.users.deleteMany({criteria}); - All he can find with such criteria
*/

db.users.deleteMany({firstName: "Bill"});

// [SECTION]  Advanced Queries

//Quert on emvedded document

db.users.find({
	contact: {
		phone: "0987654321",
		email: "stephenhawking@gmail.com"
	}
})

//If you only know email
db.users.find({
	"contact.email" : "janedoe@gmail.com"
})


//Querying an array
db.users.find({
	courses: ["CSS", "JavaScript", "Python"] //order is of significance in doing retrieval if order is wrong it will not fetch the data
})

db.users.find({
	courses: {$all: ["React", "Python"]} // order is not of significance
})

// Querying an embedded array

db.users.insertOne({
	nameArr : [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
})

db.users.find({
	namearr: 
	{
		namea: "juan"
	}
})

//insertMany to insertMany([{}])