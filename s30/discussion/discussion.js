// Aggregation in MongDB
// An aggregation operations group values from multiple documents together
// Aggregations are needed when your application needs a form of information that is not visibly available from your documents
// Aggregation USES
// You would like to know which students have not finished their courses yet
// Determine students who completed their courses
// Determine the number of students who completed specific courses

//Aggregation Pipeline - MongoDB framework for aggregation

db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: [ "Philippines", "US"]
	},

	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: [ "Philippines", "Ecuador"]
	},	

	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: [ "US", "China"]
	},	

	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: [ "Philippines", "India"]
	},	
]);

// [SECTION] MongoDB Aggregation

/*
	Used to generate manipulated data and perform operations 
	to create filtered results that helps in analyzing data
*/

// [SUB-SECTION] Using the Aggregate Method

/*
	The $match is used to pass the documents that meet the specified
	condition(s) to the next pipeline stage/aggregation process.

	SYNTAX:
	{ $match: { field: value }}

	SYNTAX:
	{ $group: { _id: "value", fieldResult: "valueResult"} }
	Using the $match and $group along with aggregation will find for products that are on sale and
	will group the total amount of stock for suppliers found.

	SYNTAX:
	db.collectionName.aggregate([
		{ $match: { fieldA: valueA }}, 
		{ $group: { _id: "$fieldB", fieldResult: { operation } }}
	])

	-The symbol $ will refer to a field name that is available in the documents 
	that are being aggregated on.
	-fieldResult can be any key you wanted to call the value generated e.g total, sum etc.
*/



db.fruits.aggregate([ 
	{ $match: { onSale: true }}, // if satisfied this criteria they will be grouped using context in $group
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock"} }} //$ sign is a must
]);


// [SUB-SECTION] Field Projection with Aggregation
/*
	-The $project can be used when aggregating data to include/exclude fields from the returned result
	-SYNTAX:
		{ $project : {field: 1/0} }	
*/

db.fruits.aggregate([ 
	{ $match: { onSale: true }}, // if satisfied this criteria they will be grouped using context in $group
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock"} }}, //$ sign is a must
	{ $project : { _id: 0} }
]);

// [ SUB-SECTION ] Sorting Aggregated Results

/*
	The $sort can be used to change the order of aggregated results
	Providing a value of -1 will sort the aggregated results in a reverse order

	-SYNTAX:
	{ $sort: { _id: 1/-1 }} //_id can be any filed you want to sort e.g total
*/

db.fruits.aggregate([ 
	{ $match: { onSale: true }}, // if satisfied this criteria they will be grouped using context in $group
	{ $group: { _id: "$supplier_id", total: { $sum: "$stock"} }}, //$ sign is a must
	{ $sort: { total : -1 } }
]);

// [ SUB-SECTION ] Aggregating Results Based on Array Fields

/*
	The $unwind deconstructs an array field from a collection/field with an array value to output
	a result for each element.

	-SYNTAX:
	 { $unwind: "field"}
*/

db.fruits.aggregate([
 	{ $unwind: "$origin"}
]);

// [ SUB-SECTION ] Display Fruits Documents by their Origin and the Kind (types?) of fruits that are Supplied.

db.fruits.aggregate([
	{ $unwind: "$origin"},
	{ $group: { _id: "$origin", kinds : { $sum: 1} }} // 1 -> to count the existence
]);