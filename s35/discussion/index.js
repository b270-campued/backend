/*Data Persistence via Mongoose ODM

What is ODM
- Object Document Mapper (Tool that translates object in code 
to document for use in document-based database (Objects format Database) e.g MongoDB)


What is Mongoose? (It's a package)
-Its ODM library that manages  data relationships, validates schemas, and 
simplifies MongoDB document manipulation via use of models (Take note MongoDB has no built in models).

Schema
-Representation of document's structure. 
-It also contains a documents expected properties and data types

Models
- Programming interface for querying or manipulate a database. A Mongoose model
contains methods that simplify such operations
*/

// npn init -y (this will do defaults)
// npm install express
// npm install mongoose
// OR npm install express mongoose

const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// changed teh admin password 
/*
	MongoDB atlas > Database Access (left nav pane) >
	look for the admin user > change password
*/

/*
	mongoose.connect() - allows our application to be connected to MongoDB (means to connect)

	useNewUrlParser : true - allows us to avoid any current and future errors while connecting to MongoDB (by default false)

	UseUnifiedTopology : true - allows us to connect to MongoDB even if the required url (MongoDB is in cloud so it changes) is updated (by default false)
	
*/
mongoose.connect("mongodb+srv://admin:admin123@zuitt.zmpg8k6.mongodb.net/b270-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		UseUnifiedTopology : true
	}
);

//connection to database 
// Prev is process of connection this one set notification if connection is a success or failure
// Identify connection status mongoose.connection
let db = mongoose.connection;

// if connection error occured, output in console 
// console.error.bind(console) allows us to print errors in the browser console as well as the terminal
// "connection error" is the message that will display if this happens
db.on("error", console.error.bind(console, "connection error"))

// if the connection is successful, output in the console "We are connected to database"
db.once("open", () => console.log("We are connected to database"));


// [SECTION] Mongoose Schema
// Schemas determine the structure of documents to be 
// written in the database; they act as blueprint of our data

/*
	Use the Schema() constructor of the mongoose module to create a new Schema object
*/
const taskSchema = new mongoose.Schema({
	//defines the fields with correspondihng data type
	// it needs a task "name" and task "status"
	// the name field requires a String data type for its value
	name: String,
	// the status field required a String data type, but since we have default: "pending",
	//users can leave this blank with a default value of "pending"
	status: {
		type: String,
		default: "pending"
	}
})

// [SECTION] Models
// Models use schemas and they act as the middleman from thr server to database 
// Server > Schema (blueprint) > Database > Collection

//"Task" variable can now be used to run commands for interactive with our database
// The naming conventions for moongoose models follows the MVC format (Capital)

// The first parameter of model method indicates the collection to store the 
// data that will be created

// The second parameter is used to specify the schema/blueprint of the document
// that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);


// [SECTION] Creation of to do list routes

//Allows our app to read json data
app.use(express.json());
// Allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

// Create a new task
// Business logic
/*
	1. Check if the task is already existing in the collection
		- if it exist, return an error/notice.
		- if it's not, we add it in the database
	2. The task will be coming from the request body
	3. Create a new task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults into "pending"
*/

// Creat the route first
// Additional note check type coercion!
app.post('/tasks', (req, res) => {
	// checking for the duplicate tasks
	// For loop can be used but findOne is exclusive to mongoose
	Task.findOne({ name : req.body.name }).then((result, err)=>{
		// if it exists, return an error/notice.
		if (result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		// if no document was found
		}else{
			// create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})
			// save the object in the collection
			newTask.save().then((savedTask, error) => {
				// try-catch-finally can also be used here
				if (error){
					return console.error(error)
				}else{
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

// Getting all task
/*
Business logic:
	1. Retrieve all the documents in the collection 
	2. If an error is encountered, print error
	3. If no errors are found, send a success (200) status back to the client/postman and return the array of the document/result
*/

app.get('/tasks', (req, res) => {
	// Retrieve all documents
	Task.find({}).then((result, error)=>{
		if (error){
			return console.log(error)
		}else{
			return res.status(200).json(result) // you can add {data : result} to add field of retrieve data
		}
	})
})

// ACTIVITY

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);

app.post('/signup', (req, res) => {
	// You will follow the collectionName used in mongoose.model() --> User
	// It is not case sensitive and automatically makes it in plural form
	User.findOne({ name : req.body.username }).then((result, err)=>{
		if (result != null && result.username === req.body.username){
			return res.send("Duplicate user found");
		}else{
			let newUser = new User({
				username : req.body.username,
				password: req.body.password
			})
			newUser.save().then((savedUser, error) => {
				if (error){
					return console.error(error)
				}else{
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at ${port}`));


// Database Connection:
// MongoDB > Go your project > Database > Overview > Connect > Drivers (Since we have app already)
// Driver = Node.js ; Version: 4.1 or later > copy connection string > Paste inside mongoose.connect();
// Take note we didn't run this in terminal npm install mongodb as we already have mongoose
// change <password> in the connection string into admin (this is your password) > In the connection string /<collectionName or database the api will manage> e.g /b270-to-do>?