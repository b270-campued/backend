// console.log("Hello World!")

// Functions
	// Functions in JS are lines/blocks of codes that tell our device/application 
	// to perform certain task when called or invoked 
	
// Function Declaration
	// A function statement defines a function with a specified parameter

	// Syntax:
	// fuction keyword (its like var, let or const keyword)
		/* function functionName() {
				codeblock (statement)
		   }
		
		-function keyword - used to define JS fuction 
		-functionName - is the fuction name. Functions are named to be able to be used or called
		later in our code. SHOULD BE DESCRIPTIVE and CAMEL CASE
		-fuction block ({}) - the statements which comprise the body of the function. This is where the code
		tp be executed is written.

		*/
		
		// Declaration
		function printName() {
			console.log("Hi! My name is John.");
		};

		// Function Invocation always with ()
		printName();

		// declaredFunction(); will return undefined function error
		// Semicolons are used to separate executable JS statements or codes.

// Function Declaration vs. Function Expression

		// 1. Function Declaration
		declaredFuction(); //declared functions can be hoisted. As long as function is declared. Top to bottom rule don't apply (hoisting)


		function declaredFuction() {
			console.log("Hello World from declaredFuction()");
		};

		declaredFuction()

		// 2. Function Expression 
			// function that can also be stored in a variable 
			// is anonymous fuction assigned to the variableFuction


			/*
				let variableName = function() {
					codeblock(statement);
				}


			*/
			let n = 30; //This is how we initialize a variable 
			// variableFunction(); hoisting is not allowed unlike function declaration - undefined

			//While this is how we initialize function expression
			let variableFunction = function() {
				console.log("Hello again Batch 270!")
			};

			variableFunction(); //We call the variable name to call the function

			//We invoke the function using the variable name not its function name
			let funcExpression = function funcName() {
				console.log("Hello from the otherside.");
			}
 
 			funcExpression(); 


 			// You can reassign declared functions and function expression to a 
 			// new anonymous fucntions.

 			declaredFuction = function() {
 				console.log("Updated declaredFuction");
 			}

 			funcExpression = function() {
 				console.log("Updated funcExpression")
 			}

 			declaredFuction();
 			funcExpression();
 			// Also take note it is only starting this line reassigned value is returned (Top to Bottom Rule)
 			// As previous calls have console.log and their value before the console.log is old (Change in logic by reassigning)

 			const constantFunc = function() {
 				console.log("Initialize with const");
 			}

 			// if const is used to function expression it cannot be reassigned
 			// Assignment to constant variable error
 			/*constantFunc = function () {
 				console.log("Cannot be re-assigned");
 			}*/ 

 			constantFunc();
 			declaredFuction();

//Function Scoping

/* 
	Scope is the accessibility/vissibility of variables within our program

		JavaScript Variables has 3 types of scope:
		1. local or block scope
		2. global scope - 
		3. function scope - function of this type can only be used inside specific function

*/
 	// Local Scope
		{
			let localVar = "Armando Perez" //Local Scope
			console.log(localVar);
		}

	// Global Scope
		let globalVar = "Mr. World"	//Global Scope

		console.log(globalVar);
		// console.log(localVar); Will not show localVar is not defined error as 
		// it can only be used inside specific local code block {}

// Function Scope

	function showNames() {
		// function scoped variables
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";
	
		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();
	
	// Error since variables can only be accessed if inside the function scope
	// console.log(functionVar)
	// console.log(functionConst)
	// console.log(functionLet)

// Nested Function

	function myNewFunction() {
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name); //Jane is ouput here since it have access to name (it considered it global as its higher)
		}
		// console.log(nestedName) nestedName is not defined error - function scope 
		nestedFunction();
	}

	myNewFunction(); // this one won't return any to return console.log(name); nestedFunction
	// should be invoked

	// nestedFunction();  nestedName is not defined error - function scope

// Function and Global Scoped Variables

	let globalName = "Alexandro";

	function myNewFunction2() {
		let nameInside = "Renz";
		console.log(globalName);
	}

	myNewFunction2();

// Using alert() - usually run immediately when page loads but 
	// can be set to run only when a function that holds it was invoked.
	// Syntax : alert("messageInString")
	// alert("Hello World!");

	function showSampleAlert() {
		alert("Hello, User!");
	}

	// showSampleAlert();

	console.log("I will only log in the console when alert is dismissed."); //top to bottom rule
	// Take note alert is comment out

// Using propmt() - used to gather info to the user 
	// Syntax: prompt("<dialogString>")

	// let samplePrompt = prompt("Enter your name.");
	// console.log("Hello, " + samplePrompt);
	// console.log(typeof samplePrompt)

	// let sampleNullPrompt = prompt("Don't enter anything.")
	// console.log(sampleNullPrompt) // This one returns empty string when there's no input (by clicking okay) 
	// But returns null when user click cancel on the prompt

	function printWelcomeMessage() {
		let firstName = prompt("Enter your firstname.");
		let lastName = prompt("Enter your lastname.");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}

	printWelcomeMessage();

// Function Naming Conventions
	//Function names should be descriptive of the task that it will perform.
	// It usually contains a verb

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}
	getCourses();

	// Avoid generic names to avoid confusion within your code.

	function get() {
		let name = "Jamie";
		console.log(name);
	}
	get();

	// Avoid pointless and inappropriate function name.

	function foo() {
		console.log(25%5);
	}

	foo();

	// Name your functions using camelCasing

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();