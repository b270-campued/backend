/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserInfo() {
		let fullName = prompt("What is your full name?");
		let age = prompt("What is your age?");
		let location = prompt("Where do you live?");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}
	getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printFiveBands() {
		let bandOne = "1. Callalily";	
		let bandTwo = "2. Rivermaya";
		let bandThree = "3. Earaserheads";
		let bandFour = "4. Hale";
		let bandFive = "5. Sandwhich";

		console.log(bandOne);
		console.log(bandTwo);
		console.log(bandThree);
		console.log(bandFour);
		console.log(bandFive);
	}

	printFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printFiveMovies() {
		let movieOne = "1. The Godfather";
		let movieOneRating = "Rotten Tomatoes Rating: 97%";
		let movieTwo = "2. The Godfather II";
		let movieTwoRating = "Rotten Tomatoes Rating: 96%";
		let movieThree = "3. Shawshank Redemption";
		let movieThreeRating = "Rotten Tomatoes Rating: 91%";
		let movieFour = "4. To Kill A Mocking Bird";
		let movieFourRating = "Rotten Tomatoes Rating: 93%";
		let movieFive = "5. Psycho";
		let movieFiveRating = "Rotten Tomatoes Rating: 96%";

		console.log(movieOne);
		console.log(movieOneRating);
		console.log(movieTwo);
		console.log(movieTwoRating);
		console.log(movieThree);
		console.log(movieThreeRating);
		console.log(movieFour);
		console.log(movieFourRating);
		console.log(movieFive);
		console.log(movieFiveRating);
	}
	printFiveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);