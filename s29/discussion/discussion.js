//Comparison Query Operator
// Query operators allow for more flexible query of data within MongoDB

//[SECTION] $gt and $gte operators
/*
	-Allows us to find documents that have field number values 
	greater than or equal to specific value
	-SYNTAX:
	db.collectionName.find({ field: { $gt : value } });
	db.collectionName.find({ field: { $gte : value } });
*/

db.users.find({ age: { $gt : 50 } });
db.users.find({ age: { $gte : 50 } });

//[SECTION] $lt and $lte operators
/*
	-Allows us to find documents that have field number values 
	less than or equal to specific value
	-SYNTAX:
	db.collectionName.find({ field: { $lt : value } });
	db.collectionName.find({ field: { $lte : value } });
*/

db.users.find({ age: { $lt : 50 } });
db.users.find({ age: { $lte : 50 } });

//[SECTION] $ne operators
/*
	-Allows us to find documents that have field number values 
	not equal to a specified value
	-SYNTAX:
	db.collectionName.find({ field: { $ne : value } });
*/

db.users.find({ age: { $ne : 82 } });

//[SECTION] $in operators
/*
	-Allows us to find documents with specific match criteria
	and can be one field using different values
	-SYNTAX:
	db.collectionName.find({ field: { $in : value } });
*/

db.users.find({ lastName: { $in : [ "Hawking", "Doe"] } });
db.users.find({ courses: { $in : [ "HTML", "React"] } });

// [SECTION] Logical Query Operators

// $or Operator

/*
	-Allows us to find documents that match a single criteria
	from multiple provided search criteria. 
	-If first criteria has no match there still second
	-SYNTAX:
	db.collectionName.find({ $or: [ {fieldA : valueA}, { fieldB : valueB } ]})
*/

db.users.find({ $or: [ {firstName : "Neil"}, { age : 25 }] });
db.users.find({ $or: [ {firstName : "Neil"}, { age : { $gt: 30 } }] });

// $and Operator
/*
	-Allows us to find documents with matching criteria in a single field
	-SYNTAX:
	db.collectionName.find({ $and: [ {fieldA : valueA}, { fieldB : valueB } ]})
*/

//Find 61-69 age range
db.users.find({ $and: [ { age : { $lt: 70} }, { age : { $gt: 60 } } ]});
db.users.find({ $and: [ { age : { $lt: 90} }, { age : { $gt: 80 } } ]});


// [SECTION] Field Projection (refers to return value from retrieval)
/*
	- Retrieving documents are common operations that we do by default
	MongoDB queries return the whole document as a reponse. 
	- When dealing with complex data structures there might be instances when field
	are not useful for the query that we are trying to accomplish.
*/

// [SECTION] Inclusion
/*
	-Allows us to include/add specific field ONLY WHEN retreiving documents.
	- The values provided is 1 to denote that the field is being inluded.
	-SYNTAX: 
	db.collectionName.find({ criteria }, {field: 1});
*/

// 1 means included for the display. Here only firstName, lastName and contact field will only show in the shell
db.users.find(
	{ firstName: "Jane" }, 
	{firstName: 1, lastName: 1, contact: 1} //This help to show in field projection what is just needed
);

// [SECTION] Exclusion

/*
	-Allows us to exclude/remove specific field ONLY WHEN retreiving documents.
	- The values provided is 0 to denote that the field is being excluded.
	-SYNTAX: 
	db.collectionName.find({ criteria }, {field: 0});
*/
db.users.find(
	{ firstName: "Jane" }, 
	{contact: 0} //This help to show in field projection what is just needed
);

// [SECTION] Supperissing the ID Field
/*
	-Allows us to exclude the "_id" fielf when retrieving documents
	SYNTAX:
	db.collectionName.find({criteria}, { _id:0});

*/

db.users.find(
	{ firstName: "Jane" }, 
	{firstName: 1, lastName: 1, contact: 1, _id: 0} //This help to show in field projection what is just needed
);

// [SECTION] Returninng Specific Fields in Embedded Documents

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

// [SECTION] Supressing Specific Fields in Embedded Documents

db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
);

// [SECTION] Evaluation Query Operators

// $regex Operator
/*
	-Allows us to find documents that match a specific string pattern 
	using regular expression.
	-SYNTAX:
	db.collectionName.find({ field: $regex : 'patern', $options: '$optionValue'});
*/

// Case Sensitive Query
db.users.find({firstName: {$regex: 'N'} });

// Case Insensitive Query
db.users.find({firstName: {$regex: 'j', $options: 'i'} }); //options disregard case sensitivity and 'i' is const of MongoDB