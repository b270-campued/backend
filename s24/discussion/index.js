// console.log("Hello, Batch 270!")

// [SECTION] ES6 Update
// ES^ is also known as ECMAScript 2015 is an update to the previous versions of ECMAScript
// ECMA - European Computer Manufacturers Association
// ECMScript is the standard used to create implementation of the language, one which is JS

// New Features of JS

// [SECTION] Exponent Operator 
const anotherNum = 8 ** 2;
console.log(anotherNum);

const num = Math.pow(8,2);
console.log(num);

// [SECTION] Template Literals

let name = "Daisy";

let message = "Hello " + name + "! \n Welcome to programming!";
console.log(message);

// String using Template Literals
// Uses backticks (``) instead of ("") and ('')

message = `Hello ${name}! Welcome to programming!`

// Multi-line Using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${anotherNum}`;
console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JS expressions
	-"${} are use to include JS expression (place holder)"

*/

const interestRate = .5;
const principal = 1000;
console.log(`The interest on your savings acount is: ${interestRate * principal}`);


// [SECTION] Array Destructuring

/*
	-Allows us to unpack elements in arrays into distinct variables
	-Allows us to name array elements with variables instead of using index number
	-Syntax:

	let/const [variableNameA, variableNameB, variablNameC] = arrayName;
*/

const fullName = ["Daisy", "Dela", "Cruz"];

//Pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you! `)


// Array destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(fullName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you! `)

// [SECTION] Object Destructuring

let person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz"
}

console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName}. It's good to see you!`)

let {givenName, midName, familyName} = person;

console.log(givenName);
console.log(midName);
console.log(familyName);

// [SECTION] Arrow Function

/*
	- Compact alternative to traditional function
	- This will only work with "function expression"

	Syntax:

	let/const variableName = () => {
		//code/block statement
	};

	let/const variableName = (parameter) => {
		//code/block statement
	};


*/

//function declaration

function greeting() {
	console.log("Hello World!")
}

greeting();

//function expression

const greet = function() {
	console.log("Hi, World!")
}

greet();

// Arrow Function 

const greet1 = () => {
	console.log("Hello Again!")
}

greet1();

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("John", "D", "Smith");

// Array Functions with loops

const students = ["John", "Jane", "Judy"];

//Pre-Arrow Function
students.forEach(function(student) {
	console.log(`${student} is a student`);
})

//Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student`);
})

// An arrow function has an implicit return value when the curly braces is omitted
// const add = (x,y) => x + y;

// normal construction
const add = (x,y) => {
	return x + y;
}

let total = add(1,2);
console.log(total);

// [SECTION] Default Function  Argument Value
//Provides a default argument value if none is provided when function is invoked

const greet2 = (name = "User") => {
	return `Good morning ${name}!`;
}

console.log(greet2());
console.log(greet2("Zoie"));


// [SECTION] Class-Based Object Blueprints

/*
	- The constructor is a special method of a class for 
	creating/initializing an object for that class
	Syntax:
	class className {
		constructor(objectPropertyA, objectPropertyB) {
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;

		}
	}

*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

// Instantiating a new object with initialized values

const myNewCar = new Car("Toyota", "Fortuner", 2020);
console.log(myNewCar);