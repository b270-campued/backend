// console.log("Hello World!")
//Introduction to Postman and REST 

// API - Application Programming Interface
// Part of server responsible for receiving request and sending responses (middle man of front and backend of application)
// What is it for?
/*
	1. Hides the server beneath an interface layer 
	 -Hidden complexity makes it easy to use
*/

// What is REST REpresentation State Transfer
// Architectural style standard (URI endpoints and HTTP methods - corresponds to CRUD) for communication (What is REST? search!)
// solves ? search!



// [SECTION] Javascript Synchronous and Asynchronous
// Javascript by default is synchronous meaning only only one statement is executed one at a time (Top to Bottom Order)

//Code blocking - waiting for specific statement to finish before executing the next statement
// for (let i = 0; i <= 1500; i++) {
// 	console.log(i);
// }

console.log("Hello Again!");

// Asynchronous means that we can proceed to execute other statements while time-consuming codes are running in the background


// [SECTION] Getting all post
// Fetch API allows us to asynchronously request for resource (data)
// It can be used to fetch various types of resources: text, JSON, HTML, images, and more
// fetch() method in JS that is used to request to a server and load info on the webpage

/*
	SYNTAX: fetch("apiURL")
*/

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status)); //used dot notation

//Returns a "promise" 
// A "promise" is an object that represents the eventual completion  or failure of an
// asynchronous function and its resulting value
	// A "promise" may be in one of 3 possible states:
	/*
		1. pending - initial state, neither fulfilled nor rejected
		2. fulfilled - operation was completed
		3. rejected - operation failed
	*/
console.log(fetch("https://jsonplaceholder.typicode.com/posts"))
// We used "json()" from the "response" object to convert the data retrived to JS Object
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
// .then(response => console.log(response))
.then(response =>{
	response.forEach(post => console.log(post.title))
})

//"async" and "await" keywords
// another approach that can be used to achieve asynchronous code

async function fetchData() {
	//waits for the fetch to complete then stores the value in the result variable
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
	console.log(result);

	//Convert result into JS Object and stores in the json variable
	let json = await result.json();
	console.log(json);
} 
fetchData();


//[SECTION] Getting a specific post
// ":id" is a wildcard where we can put any value
fetch("https://jsonplaceholder.typicode.com/posts/1") //we add specifics called wildcard /1
.then((response) => response.json())
.then((response) => console.log(response));


// [SECTION] Creating a post
/*
	SYNTAX: fetch("URL", {options})
	.then()
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response)); // response here is just parameter you can change it

// In the console id: 101 since its added and last id is 100.

// [SECTION] Updating a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Updating a post using the PATCH method
// PUT vs PATCH
	//Patch is used to update single/several properties
	//PUT is used to update whole document

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post Title",
		
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Deleta a post

fetch("https://jsonplaceholder.typicode.com/posts/1", { //still have /1 so that we cant delete all
	method: "DELETE",
})
.then(response => response.json())
.then(json => console.log(json));

